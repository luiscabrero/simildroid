<!DOCTYPE html>
<?php 
      session_start();
      if($_SESSION["nick"] != null)
	      header("Location: ../home.php");
?>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Login - SimilDroid</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<link href="style/index.css" rel="stylesheet">
	</head>
	<body>
    <!--login modal-->
	 
		<?php if($_GET["error"] === "uNF"){ ?>
			<div class="alert alert-danger">
			  <strong>E R R O R </strong> - Tu usuario y/o contrase&ntilde;a no son correctos.
			</div>
		<?php } ?>
    <div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="text-center"><img src="images/logoclrs.png" alt=""></h1>
            </div>
            <div class="modal-body">
            	<form class="form col-sm-12 center-block" action="functions/checkLogin.php" method="POST">
                  <div class="form-group">
                    <input type="text" class="form-control input-md" name="Nickname" placeholder="Nickname">
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control input-md" name="Password" placeholder="Contrase&ntilde;a">
                  </div>
                  <div class="form-group">
                    <button class="btn btn-primary btn-md btn-block" type=submit>Acceder</button>
                  </div>
                </form>
            </div>
            <div class="modal-footer">
                    <span class="pull-right"><a href="registro.php">Registrate</a></span><span class="pull-left"></span>
            </div>
        </div>
         
		  <div id="descarga" class="panel panel-default col-sm-12">
		    <a href="Otros/simildroid.apk" class="panel-body"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> </span> Descarga SimilDroid.apk</a>
		  </div>
      </div>
    </div>
	<!-- script references -->

	</body>
</html>
