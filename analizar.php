<!DOCTYPE html>
<?php 
      session_start();
      if($_SESSION["nick"] == null)
	    header("Location: ../index.php?error=nCn");
?>
<html lang="es">
    <head>
    	<title>Aplicaciones</title>
    	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!--[if IE]><link rel="shortcut icon" href="images/favicon.ico"><![endif]-->
	<link rel="icon" href="images/favicon.png">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="./style/plantilla.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="./style/analizar.css" media="screen"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<link href='https://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Sigmar+One' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Orbitron' rel='stylesheet' type='text/css'>


	<!--Pantalla de Carga -->
	
	<script src="jquery-1.3.2.min.js" type="text/javascript"></script>   
	<script>
	$(document).ready(function(){
	   $("#enlaceajax").click(function(evento){
	      evento.preventDefault();
	      $("#cargando").css("display", "inline");
	      $("#destino").load("./functions/analisis.php", function(){
	         $(location).attr('href',"./terminado.php");
	      });
	   });
	})
	</script>


    </head>
    <body>
    	<!--CABECERA-->
       <header>
		    <div class="cabecera container-fluid">
				<nav id="menu" class="navbar navbar-default">
			    	<div class="navbar-header">
			         	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			            	<span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>                        
			            </button>
			            <a  href="./home.php"><img class="navbar-brand logotipo" src="./images/logoclrs.png"></a>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
			        	<ul class="nav navbar-nav">
			            	<li><a href="home.php"><span class="glyphicon glyphicon-phone"> </span> Mis dispositivos</a></li>
			            	<li><a href="ayudanos.php"><span class="glyphicon glyphicon-bullhorn"></span> Ayudanos a mejorar</a></li>
			            	<li><a href="analizar.php"><span class="glyphicon glyphicon-tasks"> </span> Realizar an&aacute;lisis de similitud</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
			                <li class="dropdown"><a href="home.php"><?php echo $_SESSION["nick"];?></a></li>
			            	<li><a href="functions/apagar.php"><span class="cerrar glyphicon glyphicon-off"></span></a></li>
			            </ul>
			        </div>
			    </nav>
			</div>
	    </header>
	    <!--FIN CABECERA-->
	    <!--CUERPO-->
	    <div id="cuerpo" class="container-fluid">
		<div id="analiza" class="col-md-10 col-md-offset-1">
			<h2>An&aacute;lisis de aplicaciones</h2>
			<div class="box">
				<div class="descripcion">
					En este apartado se realizar&aacute; el an&aacute;lisis de similitud para TODAS las aplicaciones encontradas en tus dispositivos. Tras realizar el an&aacute;lisis, tendr&aacute;s la opci&oacute;n de realizar una peque&ntilde;a encuesta de forma que puedas decirnos si estas o no de acuerdo con la similitud encontrada 
					<img class="img-responsive" src="http://vanessatejada.com/wp-content/uploads/2015/09/herramientas-analizar-competencia.png" alt="analizar" />
				</div>			

				<div class="botonAnalizar">
			        <button id="enlaceajax" class="btn btn-primary btn-block" data-toggle="modal" data-target="#cargando">Analizar</button>
			       
					<!-- Modal -->
					<div class="modal fade" id="cargando" role="dialog" style="display:none, color:white, text-align:center">
						<div class="modal-dialog">
							<!-- Modal content-->
							<br><br>
							<img src="../images/carga.gif" class="img-responsive img-circle" alt="Cinque Terre">
							<p><center><span class="text parpadea"> C a r g a n d o </span></center></p>
						</div>
					</div>
			        
					<div id="destino"></div>
				</div>
			</div>
		</div>
	    </div>
	    <!--FIN CUERPO-->
	        
	    <!--PIE DE PAGINA-->
	    <footer>
		</footer>
	    <!--FIN PIE DE PAGINA-->
    </body>
</html>

		<script>
		$('.btn').hover(

		    function(){
			$(this).animate({'backgroundColor': 'red'},400);
		    },
		    function(){
			$(this).animate({'backgroundColor': 'yellow'},400);
		    }

		); 
		</script>
