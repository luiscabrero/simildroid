<!DOCTYPE html>
<?php
	include './functions/BBDD.php';
	session_start();
	if($_SESSION["nick"] == null)
		header("Location: ../index.php?error=nCn");
	//definimos los parámetros del apartado anterior
	$terminal = $_POST["terminal"];
	$idTerminal = $_POST["idTerminal"];
	if($terminal == null && $_SESSION["terminal"] == null)
		header("Location: ../home.php");
	else if($terminal == null && $_SESSION["terminal"] != null)
		$terminal = $_SESSION["terminal"];

?>
<html lang="es">
    <head>
    	<title>Aplicaciones</title>
    	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!--[if IE]><link rel="shortcut icon" href="images/favicon.ico"><![endif]-->
		<link rel="icon" href="images/favicon.png">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="./style/plantilla.css" media="screen"/>
		<link rel="stylesheet" type="text/css" href="./style/aplicaciones.css" media="screen"/>
      	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>
    	<!--CABECERA-->
       <header>
		    <div class="cabecera container-fluid">
				<nav id="menu" class="navbar navbar-default">
			    	<div class="navbar-header">
			         	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			            	<span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>                        
			            </button>
			            <a  href="./home.php"><img class="navbar-brand logotipo" src="./images/logoclrs.png"></a>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
			        	<ul class="nav navbar-nav">
			            	<li><a href="home.php"><span class="glyphicon glyphicon-phone"> </span> Mis dispositivos</a></li>
			            	<li><a href="ayudanos.php"><span class="glyphicon glyphicon-bullhorn"> </span> Ayudanos a mejorar</a></li>
			            	<li><a href="analizar.php"><span class="glyphicon glyphicon-tasks"> </span> Realizar an&aacute;lisis de similitud</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
			                <li class="dropdown"><a href="home.php"><?php echo $_SESSION["nick"];?></a></li>
			            	<li><a href="apagar.php"><span class="cerrar glyphicon glyphicon-off"></span></a></li>
			            </ul>
			        </div>
			    </nav>
			</div>
	    </header>
	    <!--FIN CABECERA-->
	
	    <!--CUERPO-->
	    <div id="cuerpo" class="container-fluid">

		    <div class="nuevo col-md-8 col-md-offset-3">
    	    	<h4>Aplicaciones en <?php print $terminal?></h4>
		    </div>
			
			<?php
				$BBDD = new BBDD(); 
				$listaAplicaciones = $BBDD->obtenerAplicaciones($idTerminal);
				$BBDD->terminarConexion();
			?>			
		    
		    <div class="modo col-md-5 col-md-offset-3">
				<div class="tab-content">
					<div id="todas" class="tab-pane fade in active">
						<ul class="list-group">
							<?php
								$i = 0;				
								foreach ($listaAplicaciones as $aplicacion) {
									$i = $i+1;?>
									<div class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<img src="images/aplicaciones/<?php print strtolower(substr($aplicacion['nombre'], 0,1)) ?>.png"> 
												<a data-toggle="collapse" href="#collapse<?php print $i?>"><?php print $aplicacion['nombre']?></a>
											</div>
											<div id="collapse<?php print $i?>" class="panel-collapse collapse">
												<div class="panel-body">
													<h3>Etiquetas</h3>
													
													<?php
														 $BBDD = new BBDD(); 
														 $listaEtiquetas = $BBDD->obtenerEtiquetas($idTerminal, $aplicacion['nombre']);
														 $BBDD->terminarConexion();
														 $j = 1;
														 if($listaEtiquetas != null)
														 foreach ($listaEtiquetas as $etiqueta) {
														 	
														 		print("<span>");
														 		print($etiqueta['Etiqueta_nombre']);
														 		print("</span>");
														 		if (($j % 4) == 0)
														 			print("<br><br>");
														 		$j++;
														 }
													?>
													<a data-toggle="modal" data-target="#myModal<?php print $i?>" href="#"><span class="glyphicon glyphicon-plus"></span></a>
												</div>
												<div class="panel-footer">
													<h3>Permisos</h3>
													<?php
														 $BBDD = new BBDD(); 
														 $listaPermisos = $BBDD->obtenerPermisos($idTerminal, $aplicacion['nombre']);
														 $BBDD->terminarConexion();
														 if($listaPermisos != null)
														 foreach ($listaPermisos as $permiso) {
														 		print($permiso['Permiso_nombre']);
														 		print("<br>");
														 }
													?>
												</div>
											</div>
										</div>
									</div>
										
										<!-- Modal -->
										<div class="modal fade" id="myModal<?php print $i?>" role="dialog">
											<div class="modal-dialog modal-md">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">A&ntilde;adir etiqueta a <?php print $aplicacion['nombre']?></h4>
													</div>
													<div class="modal-body">

														<?php
															$BBDD = new BBDD(); 
															$todasEtiquetas = $BBDD->obtenerEtiquetasDelUsuario($_SESSION["nick"]);
															$BBDD->terminarConexion();
														if($todasEtiquetas->num_rows != 0){
														?>
														
														Elige las etiquetas que crees que se ajustan mejor a esta aplicaci&oacute;n.
														<form action="functions/incluirEtiquetas.php" method="POST">
															 <?php	
																 if($todasEtiquetas != null)
																 foreach ($todasEtiquetas as $etiqueta) {

																 		print("<input type=\"checkbox\" name=\"etiquetas[]\" value=\"".$etiqueta['Etiqueta_nombre']."\"> ".$etiqueta['Etiqueta_nombre']."<br>");
																 }
															 ?>
															 <input class="btn btn-default" type="submit" value="A&ntilde;adir etiquetas">
															 <input type="hidden" name="idTerminal" value="<?php print $idTerminal?>"/>
															 <input type="hidden" name="tipo" value="app"/>
															 <input type="hidden" name="aplicacion" value="<?php print $aplicacion['nombre'] ?>"/>
														</form>
														<?php } ?>
														<br><br>O si lo prefieres, a&ntilde;ade una nueva etiqueta. (La etiqueta solo puede ser de una palabra, no etiquetas compuestas)
														<form action="functions/nuevaEtiqueta.php" method="POST">
															<input type="text" name="etiqueta">
															<input class="btn btn-default" type="submit" value="A&ntilde;adir">
														</form>
													</div>
												</div>
											</div>
										</div>
										<!-- Fin Modal -->
									
								<?php
								}
								?>
						</ul>
					</div>
				
				</div>
			</div>
	    </div>
	    <!--FIN CUERPO-->
	        
	    <!--PIE DE PAGINA-->
	    <footer>
		</footer>
	    <!--FIN PIE DE PAGINA-->
    </body>
</html>
