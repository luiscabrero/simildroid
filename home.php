<!DOCTYPE html>
<?php
	include './functions/BBDD.php';
	session_start();
	if($_SESSION["nick"] == null)
		header("Location: ../index.php?error=nCn");
?>
	
<html lang="es">
    <head>
    	<title>Home</title>
    	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!--[if IE]><link rel="shortcut icon" href="images/favicon.ico"><![endif]-->
		<link rel="icon" href="images/favicon.png">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="./style/plantilla.css" media="screen"/>
      	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>
    	<!--CABECERA-->
       <header>
		    <div class="cabecera container-fluid">
				<nav id="menu" class="navbar navbar-default">
			    	<div class="navbar-header">
			         	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			            	<span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>                        
			            </button>
			            <a  href="./home.php"><img class="navbar-brand logotipo" src="./images/logoclrs.png"></a>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
			        	<ul class="nav navbar-nav">
			            	<li><a href="home.php"><span class="glyphicon glyphicon-phone"> </span> Mis dispositivos</a></li>
			            	<li><a href="ayudanos.php"><span class="glyphicon glyphicon-bullhorn"> </span> Ayudanos a mejorar</a></li>
			            	<li><a href="analizar.php"><span class="glyphicon glyphicon-tasks"> </span> Realizar an&aacute;lisis de similitud</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
			                <li class="dropdown"><a href="home.php"><?php echo $_SESSION["nick"];?></a></li>
			            	<li><a href="functions/apagar.php"><span class="cerrar glyphicon glyphicon-off"></span></a></li>
			            </ul>
			        </div>
			    </nav>
			</div>
	    </header>
	    <!--FIN CABECERA-->
	
	    <!--CUERPO-->
	    <div id="cuerpo" class="container-fluid">

		    <div class="nuevo col-sm-10 col-sm-offset-1">
    	    	<h2>Mis dispositivos</h2>
		    </div>
		    <div class="tabla col-sm-offset-1 col-sm-10">
				<table class="table table-condensed">
					<thead>
						<tr>
							<th><center><center>Tipo</center></th>
							<th><center>Nombre del dispositivo</center></th>
							<th><center>Aplicaciones instaladas</center></th>
							<th><center>Etiquetas</center></th>
							<th><center>Eliminar</center></th>
						</tr>
					</thead>
					<tbody>
				    <?php
				    
						$BBDD = new BBDD(); 
						$listaTerminales = $BBDD->obtenerTerminales($_SESSION["nick"]);
						$BBDD->terminarConexion();
						
						if($listaTerminales == null)
							//Error en la busqueda
							print("Error");
						else {
							// Consulta realizada correctamente
							$i = 1;
							foreach ($listaTerminales as $terminal) {
								// code...
						    	?>
								<tr>
									<td class="nombre">
										<?php
									//	print $terminal["idTerminal"];
										print("<center><span class=\"center\">");
										print $terminal["tipo"];
								 		print("</span></center>"); 
										?>
									</td>
									<td>
										<?php
											print ("<center>".$terminal["nombre"]."</center>");
									 	?>
									</td>
									<td class="center">
										<form action="aplicaciones.php" method="POST">
											<input type="hidden" name="idTerminal" value="<?php print $terminal["idTerminal"] ?>"/>
											<input type="hidden" name="terminal" value="<?php print $terminal["nombre"] ?>"/>
											<?php 
											if($terminal["tipo"] === "movil"){?>
											<center><input type="submit" class="btn btn-primary btn-xs" value="Ver aplicaciones"></center>
											<?php 
											}
											?>
										</form>
									</td>
									<td class="center">
										<!-- Mis etiquetas Lanzador -->
										<center><button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal">Ver Etiquetas</button></center>
										
										<!-- Modal -->
										<div id="myModal" class="modal fade" role="dialog">
											<div class="modal-dialog">
											
											<!-- Mis etiquetas visualizador-->
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Etiquetas en <?php print $terminal["nombre"] ?></h4>
												</div>
												<div class="modal-body">
													<div class="alert alert-info">
														Las etiquetas tambi&eacute;n pueden asignarse a los terminales. Esto puede ayudar a realizar un an&aacute;lisis m&aacute;s exhaustivo.
													</div>
													
													Elige las etiquetas que crees que se ajustan mejor a esta aplicaci&oacute;n.
													<?php
														$BBDD = new BBDD(); 
														$todasEtiquetas = $BBDD->obtenerTodasEtiquetas();
														$BBDD->terminarConexion();
													?>
													<form action="functions/incluirEtiquetas.php" method="POST">
														 <?php	
															 if($todasEtiquetas != null)
															 foreach ($todasEtiquetas as $etiqueta) {
															 		//print($permiso['Permiso_nombre']);
															 		print("<input type=\"checkbox\" name=\"etiquetas[]\" value=\"".$etiqueta['nombre']."\"> ".$etiqueta['nombre']."<br>");
															 }
														 ?>
														 <input class="btn btn-default" type="submit" value="A&ntilde;adir etiquetas">
														 <input type="hidden" name="idTerminal" value="<?php print $terminal["idTerminal"]?>"/>
														 <input type="hidden" name="tipo" value="mvl"/>
													</form>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
											
											</div>
										</div>
									</td>
									<td class="center">
										<center><a href="eliminarDispositivo.php">Eliminar</a></center>
									</td>
								</tr>
							    <?php
							    $i+=1;
							}
						}
							
				    ?>
				    </tbody>
	           	</table>
	        </div>
			  <!-- Trigger the modal with a button -->
	        
		    <div class="nuevo col-sm-10 col-sm-offset-1">
				<button type="button" class="btn btn-sucessfull btn-block" data-toggle="modal" data-target="#myModalT"> <span class="glyphicon glyphicon-plus"> </span> A&ntilde;adir dispositivo wearable</button>
		    </div>
		    
			<!-- Modal -->
			<div id="myModalT" class="modal fade" role="dialog">
				<div class="modal-dialog">
				<!-- Modal content-->
			    <div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">A&ntilde;adir nuevo dispositivo Wearable</h4>
					</div>
					<div class="modal-body">
						<div class="alert alert-info">
						  <strong>Informaci&oacute;n</strong> Un disposivo <i>wearable</i> no es m&aacute;s que un dispositivo que se puede poner; que se lleva sobre, debajo o incluido en la ropa y que está siempre encendido, no necesita encenderse y apagarse.
						</div>
						<form method="POST" class="form-horizontal" role="form" action="functions/nuevoDispositivoWearable.php">
							<div class="form-group">
								<label class="col-sm-2 control-label">Nombre</label>
								<div class="col-sm-10">
									<input class="form-control" name="nombre" type="text" placeholder="Nombre del dispositivo" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Modelo</label>
								<div class="col-sm-10">
									<input class="form-control" name="modelo" type="text" placeholder="Modelo del dispositivo" required>
								</div>
							</div>
							
							<div class="alert alert-success">
								<strong>Ahora indique, de esta lista, qu&eacute; aplicaciones est&aacute;n conectadas con el nuevo dispositivo.</strong>
							</div>
							
							<div class="checkbox col-md-12">
							<?php
								 $BBDD = new BBDD(); 
								 $terminales = $BBDD->obtenerTerminales($_SESSION["nick"]);
								 $BBDD->terminarConexion();
								 
								 if($terminales != null)
								 foreach ($terminales as $terminal) {
									if($terminal["tipo"] === "movil"){
								 		print("<center><b>Aplicaciones en tu <i>".$terminal["nombre"]."</i></b></center>");
										$BBDD = new BBDD(); 
										$apps = $BBDD->obtenerAplicaciones($terminal["idTerminal"]);
										$BBDD->terminarConexion();
										if($apps != null)
										foreach($apps as $app)
								 			print ("<label class=\"col-md-4\">
								 				        <input name=\"apps[]\" type=\"checkbox\" value=\"".$app["nombre"]."-".$terminal["idTerminal"]."\">".$app["nombre"]."
								 			        </label>");
									}
								 }
							?>
							</div>
						    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancelar</button>
							<button type="submit" class="btn btn-success">A&ntilde;adir</button>
  						</form>
			        
			      </div>
			    </div>
			
			  </div>
			</div> 

	    </div>
	    <!--FIN CUERPO-->
	        
	    <!--PIE DE PAGINA-->
	    <footer>
		</footer>
	    <!--FIN PIE DE PAGINA-->
    </body>
</html>
