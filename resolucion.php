<!DOCTYPE html>
<?php
	include './functions/BBDD.php'; //incluimos este fichero para poder hacer llamadas a la base de datos.
        include './functions/resolucionObjects.php'; //incluimos este fichero para poder crear objetos de tipo cluster
	function leerResultado(){
		$resultado = array(
			"nombre" => array(),
			"terminal" => array(),
			"cluster" => array()
		);
		$file = fopen("./functions/tmp/solucion.csv", "r"); //abrimos el fichero dnde esta la solucion de R
		while(!feof($file)){
			$linea = fgets($file);
			if($linea != ""){
				$res = explode(";", $linea); //donde res[0] == nombre de la app y res[1] == numero de cluster
				$dirtyName = $res[0]; //nombre con comillas y numero de terminal
				$nameANDTerminal = str_replace("\"", "", $dirtyName);// nombre con numero de terminal
				$separador = strripos($nameANDTerminal,"-");
				$name = substr($nameANDTerminal, 0, $separador);
				$terminal = substr($nameANDTerminal, $separador+1); 
				
				array_push($resultado["cluster"],$res[1]);
				array_push($resultado["nombre"],$name);				
				array_push($resultado["terminal"],$terminal);

			}
		}
		$clusters = array();		
		
		$numeroDeClusters = 0;
		foreach($resultado["cluster"] as $r){
			if((int)$r > (int)$numeroDeClusters)
				$numeroDeClusters = $r;	
		}

		for($i = 0; $i < $numeroDeClusters; $i++){
			$aplicaciones = array();
		
			for($j = 0; $j < sizeof($resultado["cluster"]); $j++){
				if((int)$resultado["cluster"][$j]-1 == $i){
					$aplicacion = new Aplicacion();
					$aplicacion->nombre = $resultado["nombre"][$j];
					$aplicacion->terminalAsociado = $resultado["terminal"][$j];
					array_push($aplicaciones, $aplicacion);
				}
			}
			$cluster = array(
				"nombre" => $i+1,
				"aplicaciones" => $aplicaciones
			);
			array_push($clusters, $cluster);	
		}
		
		//antes de devolver la informacion para mostrarla, vamos a almacenar esta estructura en la base de datos para poder luego compararla desde la parte de administracion
		//print json_encode($clusters);
		$BBDD = new BBDD();
		$almacenado = $BBDD->almacenarResolucionR($_SESSION["nick"], json_encode($clusters)); //almacenamos la resolución que se devuelve al usuario
		$BBDD->terminarConexion();

    		return $clusters;
	}
	
	session_start();
	if($_SESSION["nick"] == null)
		header("Location: ../index.php?error=nCn");
?>
	
<html lang="es">
    <head>
    	<title>Resoluci&oacute;n de an&aacute;lisis</title>
    	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!--[if IE]><link rel="shortcut icon" href="images/favicon.ico"><![endif]-->
		<link rel="icon" href="images/favicon.png">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="./style/plantilla.css" media="screen"/>
      	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	  <style>

		.lista ul
		{
		   padding-right: 0px;
		   padding-left: 0px;
		   padding-bottom: 0px;
		   padding-top: 0px;
		   margin: 0px;
		}
		.lista li
		{
		   list-style-type: none;
		   display: inline;
		   padding-right: 5px;
		   background:#a3c339;
		   margin-left: 20px;
		}
		.panel {
		   padding:0px;
		}
		.tabla{
		   margin-top:50px;		
		{
	  </style>
    <?php $resultado = leerResultado(); ?>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="./js/<?php print sizeof($resultado);?>.js"></script>
    </head>
    <body>
    	<!--CABECERA-->
       <header>
		<div class="cabecera container-fluid">
			<nav id="menu" class="navbar navbar-default">
			    	<div class="navbar-header">
			         	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					    	<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>                        
			            	</button>
			            	<a  href="./home.php"><img class="navbar-brand logotipo" src="./images/logoclrs.png"></a>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
					    	<li><a href="home.php"><span class="glyphicon glyphicon-phone"> </span> Mis dispositivos</a></li>
					    	<li><a href="ayudanos.php"><span class="glyphicon glyphicon-bullhorn"> </span> Ayudanos a mejorar</a></li>
					    	<li><a href="analizar.php"><span class="glyphicon glyphicon-tasks"> </span> Realizar an&aacute;lisis de similitud</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown"><a href="home.php"><?php echo $_SESSION["nick"];?></a></li>
					    	<li><a href="functions/apagar.php"><span class="cerrar glyphicon glyphicon-off"></span></a></li>
				    	</ul>
		        	</div>
		    	</nav>
		</div>
	    </header>
	    <!--FIN CABECERA-->
	
	    <!--CUERPO-->
	    <div id="cuerpo" class="container-fluid">
		<div class=" col-sm-10 col-sm-offset-1 centered">
			<h2>Resoluci&oacute;n de an&aacute;lisis</h2>
		</div>

		<div class="tabla col-sm-10 col-sm-offset-1 centered">
		 Despu&eacute;s de estudiar tus aplicaciones, hemos llegado a la conclusi&oacute;n de que todas ellas pueden dividirse en <?php print sizeof($resultado) ?> grupos distintos. Nuestro sistema ha seleccionado un orden concreto para todas estas aplicaciones, por favor, si crees que alguna aplicaci&oacute;n no est&aacute; en el grupo que debería, ind&iacute;calo cambiando el n&uacute;mero (puedes dejar grupos vac&iacute;os si lo consideras oportuno).	
		</div><br><br>
		<div class="tabla col-sm-offset-1 col-sm-11">
		 
		<?php
			print "<form id=\"formulario\" action=\"./functions/almacenarFeedBack.php\" method=\"post\">";
				foreach($resultado as $r){
					print "<div class=\"panel col-md-10 panel-primary\">";
						print "<div class=\"panel-heading\"><h3> Grupo n&uacute;mero ".$r["nombre"]."</h3></div>";
						print "<div class=\"panel-body lista\">";
							print "<ul id=\"sortable".$r["nombre"]."\" class=\"connectedSortable\">";
								for($j = 0; $j < sizeof($r["aplicaciones"]); $j++){
									print "<li id='".$r["aplicaciones"][$j]->nombre."' class='ui-state-highlight'>".$r["aplicaciones"][$j]->nombre."</li>";
								}

							print "</ul>";
						print "</div>";
					print "</div>";
				
				}
				print "<input type = 'hidden' type='text' name='numClusters' value='".sizeof($resultado)."'>";
				for($j = 0; $j < sizeof($resultado); $j++){
					print "<input type = 'hidden' id='input".($j+1)."' type='text' name='".($j+1)."' value=''>";
				}
				print "<input class='col-md-10 btn-success' type='submit'>";
			print "</form>";
		?>
		</div> 
	    </div>
	    <!--FIN CUERPO-->
	        
	    <!--PIE DE PAGINA-->
	    <footer>
		</footer>
	    <!--FIN PIE DE PAGINA-->
    </body>
</html>
