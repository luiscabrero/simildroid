 Version 2.1.13 can access:
Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Wi-Fi connection information

    view Wi-Fi connections

Other

    full network access
    Google Play license check
    prevent device from sleeping
    view network connections

Updates to Little Fox Music Box may automatically add additional capabilities within each group. Learn more
