
This app has access to:
In-app purchases

Identity

    find accounts on the device

Contacts

    find accounts on the device

Location

    approximate location (network-based)

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Wi-Fi connection information

    view Wi-Fi connections

Other

    receive data from Internet
    control vibration
    run at startup
    prevent device from sleeping
    view network connections
    full network access
    connect and disconnect from Wi-Fi
    Google Play license check

Updates to UNO � & Friends may automatically add additional capabilities within each group. Learn more
