 Version 1.4 can access:
Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Other

    full network access
    view network connections

Updates to Smartwatch Center Android Wear may automatically add additional capabilities within each group. Learn more
