
This app has access to:
Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Camera

    take pictures and videos

Other

    full network access

Updates to Boomerang from Instagram may automatically add additional capabilities within each group. Learn more
