Version 4.0.2 can access:
In-app purchases

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Other

    full network access
    view network connections
    run at startup

Updates to Period Calendar, Cycle Tracker may automatically add additional capabilities within each group. Learn more
