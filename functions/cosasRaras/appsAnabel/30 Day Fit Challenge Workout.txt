 Version 1.0.7 can access:
In-app purchases

Identity

    find accounts on the device

Contacts

    find accounts on the device

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Other

    control vibration
    use accounts on the device
    full network access
    prevent device from sleeping
    view network connections

Updates to 30 Day Fit Challenge Workout may automatically add additional capabilities within each group. Learn more
