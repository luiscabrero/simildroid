 Version 9.8.000000002471 can access:
Identity

    find accounts on the device
    read your own contact card
    add or remove accounts

Contacts

    find accounts on the device
    read your contacts
    modify your contacts

Location

    precise location (GPS and network-based)
    approximate location (network-based)

SMS

    receive text messages (SMS)
    read your text messages (SMS or MMS)
    send SMS messages

Phone

    read call log
    directly call phone numbers
    read phone status and identity

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Camera

    take pictures and videos

Microphone

    record audio

Wi-Fi connection information

    view Wi-Fi connections

Device ID & call information

    read phone status and identity

Other

    receive data from Internet
    full network access
    view network connections
    control vibration
    draw over other apps
    run at startup
    install shortcuts
    prevent device from sleeping
    change your audio settings
    pair with Bluetooth devices
    send sticky broadcast
    control Near Field Communication
    read Google service configuration
    read sync settings
    toggle sync on and off
    create accounts and set passwords

Updates to imo free video calls and chat may automatically add additional capabilities within each group. Learn more
