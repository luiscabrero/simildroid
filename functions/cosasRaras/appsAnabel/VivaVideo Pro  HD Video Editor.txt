 Version 4.5.8 can access:
Device & app history

    retrieve running apps

Location

    approximate location (network-based)

Photos/Media/Files

    modify or delete the contents of your USB storage
    access USB storage filesystem
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Camera

    take pictures and videos

Microphone

    record audio

Wi-Fi connection information

    view Wi-Fi connections

Other

    prevent device from sleeping
    control vibration
    full network access
    view network connections
    modify system settings
    run at startup

Updates to VivaVideo Pro: HD Video Editor may automatically add additional capabilities within each group. Learn more
