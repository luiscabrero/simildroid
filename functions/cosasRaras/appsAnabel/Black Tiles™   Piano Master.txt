 Version 1.0.91 can access:
In-app purchases

Identity

    find accounts on the device

Contacts

    find accounts on the device

Location

    precise location (GPS and network-based)

Phone

    read phone status and identity

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Wi-Fi connection information

    view Wi-Fi connections

Device ID & call information

    read phone status and identity

Other

    full network access
    view network connections
    run at startup

Updates to Black Tiles� : Piano Master may automatically add additional capabilities within each group. Learn more
