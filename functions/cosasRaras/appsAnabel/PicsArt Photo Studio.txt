
This app has access to:
In-app purchases

Device & app history

    retrieve running apps

Identity

    find accounts on the device

Contacts

    find accounts on the device
    read your contacts

Location

    precise location (GPS and network-based)
    approximate location (network-based)

Phone

    read phone status and identity

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Camera

    take pictures and videos

Wi-Fi connection information

    view Wi-Fi connections

Device ID & call information

    read phone status and identity

Other

    bind to a wallpaper
    receive data from Internet
    install shortcuts
    read Google service configuration
    prevent device from sleeping
    view network connections
    set wallpaper
    full network access

Updates to PicsArt Photo Studio may automatically add additional capabilities within each group. Learn more
