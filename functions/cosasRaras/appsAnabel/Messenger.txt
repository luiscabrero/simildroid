
This app has access to:
Identity

    find accounts on the device
    read your own contact card
    add or remove accounts

Contacts

    find accounts on the device
    read your contacts
    modify your contacts

Location

    precise location (GPS and network-based)
    approximate location (network-based)

SMS

    edit your text messages (SMS or MMS)
    receive text messages (SMS)
    send SMS messages
    read your text messages (SMS or MMS)
    receive text messages (MMS)

Phone

    read phone status and identity
    read call log
    directly call phone numbers
    reroute outgoing calls

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Camera

    take pictures and videos

Microphone

    record audio

Wi-Fi connection information

    view Wi-Fi connections

Device ID & call information

    read phone status and identity

Other

    receive data from Internet
    download files without notification
    control vibration
    run at startup
    draw over other apps
    pair with Bluetooth devices
    send sticky broadcast
    create accounts and set passwords
    change network connectivity
    prevent device from sleeping
    install shortcuts
    read battery statistics
    read sync settings
    toggle sync on and off
    read Google service configuration
    view network connections
    change your audio settings
    full network access

Updates to Messenger may automatically add additional capabilities within each group. Learn more
