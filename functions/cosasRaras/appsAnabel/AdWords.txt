 Version 1.5.7 can access:
Identity

    find accounts on the device

Contacts

    find accounts on the device

Other

    receive data from Internet
    use accounts on the device
    view network connections
    full network access
    prevent device from sleeping
    read Google service configuration

Updates to AdWords may automatically add additional 