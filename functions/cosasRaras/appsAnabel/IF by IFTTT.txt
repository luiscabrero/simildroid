 Version 1.6.2 can access:
Identity

    find accounts on the device
    add or remove accounts

Contacts

    read your contacts
    find accounts on the device

Location

    precise location (GPS and network-based)

SMS

    read your text messages (SMS or MMS)
    receive text messages (SMS)
    receive text messages (MMS)
    send SMS messages

Phone

    read phone status and identity
    read call log

Photos/Media/Files

    read the contents of your USB storage
    modify or delete the contents of your USB storage

Storage

    read the contents of your USB storage
    modify or delete the contents of your USB storage

Wi-Fi connection information

    view Wi-Fi connections

Device ID & call information

    read phone status and identity

Other

    receive data from Internet
    full network access
    run at startup
    toggle sync on and off
    set wallpaper
    modify system settings
    connect and disconnect from Wi-Fi
    pair with Bluetooth devices
    access Bluetooth settings
    disable your screen lock
    view network connections
    control vibration
    prevent device from sleeping
    create accounts and set passwords

Updates to IF by IFTTT may automatically add additional capabilities within each group. Learn more
