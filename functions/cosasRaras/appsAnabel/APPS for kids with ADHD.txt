 Version 1.0.20 can access:
Device & app history

    retrieve running apps

Identity

    find accounts on the device

Contacts

    find accounts on the device

Location

    approximate location (network-based)

Phone

    read phone status and identity

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Device ID & call information

    read phone status and identity

Other

    full network access
    view network connections
    control vibration
    pair with Bluetooth devices

Updates to APPS for kids with ADHD may automatically add additional capabilities within each group. Learn more
