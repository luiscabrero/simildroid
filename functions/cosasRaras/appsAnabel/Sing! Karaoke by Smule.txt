 Version 3.8.5 can access:
In-app purchases

Device & app history

    retrieve running apps

Identity

    find accounts on the device

Contacts

    find accounts on the device

Location

    approximate location (network-based)

Phone

    read phone status and identity

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Camera

    take pictures and videos

Microphone

    record audio

Wi-Fi connection information

    view Wi-Fi connections

Device ID & call information

    read phone status and identity

Other

    receive data from Internet
    full network access
    view network connections
    change your audio settings
    prevent device from sleeping
    use accounts on the device

Updates to Sing! Karaoke by Smule may automatically add additional capabilities within each group. Learn more
