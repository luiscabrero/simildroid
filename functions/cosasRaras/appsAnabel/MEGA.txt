 Version 3.0.11.2 can access:
In-app purchases

Contacts

    read your contacts

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Camera

    take pictures and videos

Other

    full network access
    view network connections
    prevent device from sleeping

Updates to MEGA may automatically add additional capabilities within each group. Learn more
