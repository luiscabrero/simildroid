
This app has access to:
In-app purchases

Phone

    read phone status and identity

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Device ID & call information

    read phone status and identity

Other

    disable your screen lock
    full network access
    prevent device from sleeping
    view network connections

Updates to slither.io may automatically add additional capabilities within each group. Learn more
