
This app has access to:
Identity

    add or remove accounts
    find accounts on the device

Contacts

    find accounts on the device
    read your contacts

Location

    precise location (GPS and network-based)
    approximate location (network-based)

SMS

    receive text messages (SMS)

Phone

    read phone status and identity

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Camera

    take pictures and videos

Microphone

    record audio

Wi-Fi connection information

    view Wi-Fi connections

Device ID & call information

    read phone status and identity

Other

    receive data from Internet
    read Home settings and shortcuts
    install shortcuts
    control vibration
    use accounts on the device
    access Bluetooth settings
    control flashlight
    pair with Bluetooth devices
    connect and disconnect from Wi-Fi
    create accounts and set passwords
    prevent device from sleeping
    view network connections
    full network access

Updates to Amazon Shopping may automatically add additional capabilities within each group. Learn more
