 Version 5.0.5 can access:
Identity

    find accounts on the device

Contacts

    find accounts on the device

Location

    approximate location (network-based)
    precise location (GPS and network-based)

Phone

    read phone status and identity
    directly call phone numbers

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Camera

    take pictures and videos

Device ID & call information

    read phone status and identity

Other

    receive data from Internet
    prevent device from sleeping
    full network access
    view network connections
    control vibration

Updates to McDonald's Espa�a may automatically add additional capabilities within each group. Learn more
