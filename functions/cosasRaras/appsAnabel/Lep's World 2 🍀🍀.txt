
This app has access to:
In-app purchases

Phone

    read phone status and identity

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Wi-Fi connection information

    view Wi-Fi connections

Device ID & call information

    read phone status and identity

Other

    full network access
    set an alarm
    prevent device from sleeping
    view network connections

Updates to Lep's World 2 🍀🍀 may automatically add additional capabilities within each group. Learn more
