
This app has access to:
In-app purchases

Identity

    add or remove accounts
    find accounts on the device

Contacts

    find accounts on the device

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Wi-Fi connection information

    view Wi-Fi connections

Other

    receive data from Internet
    toggle sync on and off
    prevent device from sleeping
    create accounts and set passwords
    view network connections
    draw over other apps
    modify system settings
    use accounts on the device
    full network access
    send sticky broadcast

Updates to Scribd - A World of Books may automatically add additional capabilities within each group. Learn more
