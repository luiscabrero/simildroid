 Version 1.1.60 can access:
In-app purchases

Identity

    find accounts on the device

Contacts

    find accounts on the device

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Wi-Fi connection information

    view Wi-Fi connections

Other

    receive data from Internet
    full network access
    control vibration
    prevent device from sleeping
    view network connections
    use accounts on the device
    Google Play license check

Updates to Plants vs. Zombies FREE may automatically add additional capabilities within each group. Learn more
