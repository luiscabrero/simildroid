
This app has access to:
In-app purchases

Location

    approximate location (network-based)
    precise location (GPS and network-based)

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Wi-Fi connection information

    view Wi-Fi connections

Other

    receive data from Internet
    control vibration
    run at startup
    read Google service configuration
    prevent device from sleeping
    view network connections
    full network access

Updates to 1Weather:Widget Forecast Radar may automatically add additional capabilities within each group. Learn more
