 Version 2.1.33 can access:
Identity

    find accounts on the device
    read your own contact card
    add or remove accounts

Calendar

    add or modify calendar events and send email to guests without owners' knowledge
    read calendar events plus confidential information

Contacts

    find accounts on the device
    read your contacts
    modify your contacts

Location

    precise location (GPS and network-based)

Phone

    read phone status and identity
    directly call phone numbers

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Device ID & call information

    read phone status and identity

Other

    receive data from Internet
    read sync statistics
    create accounts and set passwords
    control vibration
    toggle sync on and off
    use accounts on the device
    prevent device from sleeping
    view network connections
    read sync settings
    full network access

Updates to Microsoft Outlook may automatically add additional capabilities within each group. Learn more


    recibir datos de Internet
    leer estadísticas de sincronización
    crear cuentas y establecer contraseñas
    controlar la vibración
    activar y desactivar la sincronización
    usar cuentas del dispositivo
    impedir que el dispositivo entre en modo de suspensión
    ver conexiones de red
    leer la configuración de sincronización
    acceso completo a red

Instalar actualizaciones de Microsoft Outlook puede añadir automáticamente funciones adicionales dentro de cada grupo. Más información
