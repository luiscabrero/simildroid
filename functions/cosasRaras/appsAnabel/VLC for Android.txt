
This app has access to:
Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Other

    control vibration
    run at startup
    change your audio settings
    modify system settings
    full network access
    prevent device from sleeping
    view network connections
    draw over other apps

Updates to VLC for Android may automatically add additional capabilities within each group. Learn more
