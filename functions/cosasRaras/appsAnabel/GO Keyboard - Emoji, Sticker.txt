
This app has access to:
In-app purchases

Device & app history

    read sensitive log data
    retrieve running apps

Identity

    find accounts on the device

Contacts

    find accounts on the device
    read your contacts

Phone

    read call log

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Microphone

    record audio

Other

    power device on or off
    display unauthorized windows
    control vibration
    run at startup
    prevent device from sleeping
    view network connections
    set wallpaper
    draw over other apps
    read terms you added to the dictionary
    full network access
    add words to user-defined dictionary

Updates to GO Keyboard - Emoji, Sticker may automatically add additional capabilities within each group. Learn more
