
This app has access to:
Device & app history

    retrieve running apps

Identity

    find accounts on the device

Contacts

    find accounts on the device

Location

    approximate location (network-based)
    precise location (GPS and network-based)

SMS

    edit your text messages (SMS or MMS)

Phone

    read phone status and identity
    directly call phone numbers

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Wi-Fi connection information

    view Wi-Fi connections

Device ID & call information

    read phone status and identity

Other

    receive data from Internet
    mock location sources for testing
    prevent device from sleeping
    expand/collapse status bar
    view network connections
    set wallpaper
    change system display settings
    modify system settings
    full network access
    send sticky broadcast
    connect and disconnect from Wi-Fi

Updates to Foster's Hollywood may automatically add additional capabilities within each group. Learn more
