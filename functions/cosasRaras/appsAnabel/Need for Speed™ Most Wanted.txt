 Version 1.3.71 can access:
In-app purchases

Identity

    find accounts on the device

Contacts

    read your contacts
    find accounts on the device

Phone

    read phone status and identity

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Wi-Fi connection information

    view Wi-Fi connections

Device ID & call information

    read phone status and identity

Other

    receive data from Internet
    full network access
    view network connections
    change network connectivity
    prevent device from sleeping
    Google Play license check

Updates to Need for Speed� Most Wanted may automatically add additional capabilities within each group. Learn more
