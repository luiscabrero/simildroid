 Version 2.3.4 can access:
In-app purchases

Device & app history

    read sensitive log data

Phone

    read phone status and identity

Photos/Media/Files

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Storage

    modify or delete the contents of your USB storage
    read the contents of your USB storage

Microphone

    record audio

Wi-Fi connection information

    view Wi-Fi connections

Device ID & call information

    read phone status and identity

Other

    full network access
    run at startup
    prevent device from sleeping

Updates to Hue Pro may automatically add additional capabilities within each group. Learn more
