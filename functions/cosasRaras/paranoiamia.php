<?php

	//$hoy = getdate(); //obtenemos fecha para crear el fichero personalizado
	$i = 0;
	while ($i < 200){
	    $file = fopen("../js/".$i.".js", "w"); //abrimos el fichero donde vamos a generar el contenido del GraphML

	    //generamos la parte común del fichero, todos los GML tendrán estas primeras líneas comunes.
		$sortable = "";		
		for($j = 0; $j < $i; $j++){
			$sortable= $sortable."#sortable".($j+1);
			if($j != $i - 1)
				$sortable= $sortable.", ";
		}
		print $sortable."<br>";
	    fwrite($file, "	$(function(){
		$(\"".$sortable."\" ).sortable({
			connectWith: '.connectedSortable'
		});
	
		$(\"#formulario\" ).submit(function( event ) {".PHP_EOL);
		
		for($j = 0; $j < $i; $j++){
			fwrite($file, "			var newOrde".($j+1)." = $(this).find(\"#sortable".($j+1)."\").sortable('toArray').toString();".PHP_EOL);
			fwrite($file, "			$(\"#input".($j+1)."\").attr(\"value\",newOrde".($j+1).");".PHP_EOL);
		}
		fwrite($file, "		});
	});".PHP_EOL);	
	    $i++;
	}
?>
