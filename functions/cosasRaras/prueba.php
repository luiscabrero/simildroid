<?php
    $directorio = './appsAnabel/';
    $ficheros1  = scandir($directorio);
    foreach($ficheros1 as $fichero){
        $file = fopen("./appsAnabel/".$fichero, "r");
        $file_MOD = fopen("./appsAnabel/0_MOD".$fichero, "w");
    
        while(!feof($file)){
            
            $linea = fgets($file);
            if(
            strpos($linea, "measure app storage space")+
            strpos($linea, "erase USB storage")+
            strpos($linea, "use accounts on the device")+
            strpos($linea, "access USB storage filesystem")+
            strpos($linea, "change network connectivity")+
            
            
            strpos($linea, "run at startup")+
            strpos($linea, "read Google service configuration")+
            strpos($linea, "view network connections")+
            strpos($linea, "read your Web bookmarks and history")+
            strpos($linea, "retrieve running apps")+
            strpos($linea, "update component usage statistics")+
            strpos($linea, "read Home settings and shortcuts")+
            strpos($linea, "draw over other apps")+
            strpos($linea, "write web bookmarks and history")+
            strpos($linea, "create accounts and set passwords")+
            strpos($linea, "control Near Field Communication")+
            strpos($linea, "write subscribed feeds")+
            strpos($linea, "disable your screen lock")+
            strpos($linea, "add words to user-defined dictionary")+
            strpos($linea, "write Home settings and shortcuts")+
            strpos($linea, "Access download manager")
            ){
            
            }
            else if(strpos($linea, "find accounts on the device")){
                fwrite($file_MOD, "GET_ACCOUNTS".PHP_EOL); 
            }
            else if(strpos($linea, "precise location (GPS and network-based)")){
                fwrite($file_MOD, "ACCESS_FINE_LOCATION".PHP_EOL); 
            }
            else if(strpos($linea, "approximate location (network-based)")){
                fwrite($file_MOD, "ACCESS_COARSE_LOCATION".PHP_EOL); 
            }
            else if(strpos($linea, "record audio")){
                fwrite($file_MOD, "RECORD_AUDIO".PHP_EOL); 
            }
            else if(strpos($linea, "pair with Bluetooth devices")){
                fwrite($file_MOD, "BLUETOOTH".PHP_EOL); 
            }
            else if(strpos($linea, "access Bluetooth settings")){
                fwrite($file_MOD, "BLUETOOTH_ADMIN".PHP_EOL); 
            }
            else if(strpos($linea, "take pictures and videos")){
                fwrite($file_MOD, "CAMERA".PHP_EOL); 
            }
            else if(strpos($linea, "receive data from Internet")){
                fwrite($file_MOD, "INTERNET".PHP_EOL); 
            }
            else if(strpos($linea, "change your audio settings")){
                fwrite($file_MOD, "MODIFY_AUDIO_SETTINGS".PHP_EOL); 
            }
            else if(strpos($linea, "read the contents of your USB storage")){
                fwrite($file_MOD, "READ_EXTERNAL_STORAGE".PHP_EOL); 
            }
            else if(strpos($linea, "read phone status and identity")){
                fwrite($file_MOD, "READ_PHONE_STATE".PHP_EOL); 
            }
            else if(strpos($linea, "read sync settings")){
                fwrite($file_MOD, "READ_SYNC_SETTINGS".PHP_EOL); 
            }
            else if(strpos($linea, "control vibration")){
                fwrite($file_MOD, "VIBRATE".PHP_EOL); 
            }
            else if(strpos($linea, "prevent device from sleeping")){
                fwrite($file_MOD, "WAKE_LOCK".PHP_EOL); 
            }
            else if(strpos($linea, "modify or delete the contents of your USB storage")){
                fwrite($file_MOD, "WRITE_EXTERNAL_STORAGE".PHP_EOL); 
            }
            else if(strpos($linea, "toggle sync on and off")){
                fwrite($file_MOD, "WRITE_SYNC_SETTINGS".PHP_EOL); 
            }
            else if(strpos($linea, "add or remove accounts")){
                fwrite($file_MOD, "ACCOUNT_MANAGER".PHP_EOL); 
            }
            else if(strpos($linea, "view Wi-Fi connections")){
                fwrite($file_MOD, "ACCESS_WIFI_STATE".PHP_EOL); 
            }
            else if(strpos($linea, "full network access")){
                fwrite($file_MOD, "CHANGE_NETWORK_STATE".PHP_EOL); 
            }
            else if(strpos($linea, "read your contacts")){
                fwrite($file_MOD, "READ_CONTACTS".PHP_EOL); 
            }
            else if(strpos($linea, "modify your contacts")){
                fwrite($file_MOD, "WRITE_CONTACTS".PHP_EOL); 
            }
            else if(strpos($linea, "edit your text messages (SMS or MMS)")+strpos($linea, "send SMS messages")){
                fwrite($file_MOD, "SEND_SMS".PHP_EOL); 
            }
            else if(strpos($linea, "receive text messages (SMS)")){
                fwrite($file_MOD, "RECEIVE_SMS".PHP_EOL); 
            }
            else if(strpos($linea, "read your text messages (SMS or MMS)")){
                fwrite($file_MOD, "READ_SMS".PHP_EOL); 
            }
            else if(strpos($linea, "write call log")){
                fwrite($file_MOD, "WRITE_CALL_LOG".PHP_EOL); 
            }
            else if(strpos($linea, "read call log")){
                fwrite($file_MOD, "READ_CALL_LOG".PHP_EOL); 
            }
            else if(strpos($linea, "directly call phone numbers")){
                fwrite($file_MOD, "CALL_PHONE".PHP_EOL); 
            }
            else if(strpos($linea, "delete other apps' data")){
                fwrite($file_MOD, "DELETE_PACKAGES".PHP_EOL); 
            }
            else if(strpos($linea, "connect and disconnect from Wi-Fi")){
                fwrite($file_MOD, "CHANGE_NETWORK_STATE".PHP_EOL); 
            }
            else if(strpos($linea, "close other apps")){
                fwrite($file_MOD, "KILL_BACKGROUND_PROCESSES".PHP_EOL); 
            }
            else if(strpos($linea, "install shortcuts")){
                fwrite($file_MOD, "INSTALL_SHORTCUT".PHP_EOL); 
            }
            else if(strpos($linea, "expand/collapse status bar")){
                fwrite($file_MOD, "EXPAND_STATUS_BAR".PHP_EOL); 
            }
            else if(strpos($linea, "set an alarm")){
                fwrite($file_MOD, "SET_ALARM".PHP_EOL); 
            }
            else if(strpos($linea, "delete all app cache data")){
                fwrite($file_MOD, "CLEAR_APP_CACHE".PHP_EOL); 
            }
            else if(strpos($linea, "uninstall shortcuts")){
                fwrite($file_MOD, "UNINSTALL_SHORTCUT".PHP_EOL); 
            }
            else if(strpos($linea, "read sensitive log data")){
                fwrite($file_MOD, "READ_LOGS".PHP_EOL); 
            }
            else if(strpos($linea, "add or modify calendar events and send email to ")){
                fwrite($file_MOD, "WRITE_CALENDAR".PHP_EOL); 
            }
            else if(strpos($linea, "uninstall shortcuts")){
                fwrite($file_MOD, "UNINSTALL_SHORTCUT".PHP_EOL); 
            }
            else if(strpos($linea, "modify system settings")){
                fwrite($file_MOD, "WRITE_SETTINGS".PHP_EOL); 
            }
            else if(strpos($linea, "read battery statistics")){
                fwrite($file_MOD, "BATTERY_STATS".PHP_EOL); 
            }
            else{
                print("En la app ".$fichero." no sabemos lo que es: ".$linea."<br>");
            }
            
        }
        
        fclose($file);
        fclose($file_MOD);
    }
?>