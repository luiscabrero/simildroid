<?php

$dbhost = "localhost";
$database="mydb";
$user="root";
$password="";

// Creacion de conexion
$conn = new mysqli($dbhost, $user, $password, $database);

// Comprobacion de conexion 
if ($conn->connect_error) {
    die("Error en la conexion a MySQL: " . $conn->connect_error);
}

//definimos los parámetros del apartado anterior
$nick = $_POST["Nickname"];
$msg = $_POST["mensaje"];

//buscamos los mensajes escritos por el usuario
if($stmt = $conn->prepare("SELECT * FROM Mensaje WHERE Usuario_nick = ?")){
    $stmt->bind_param("s", $nick);
    //ejecutamos la consulta
    $stmt->execute();
    
    $stmt->store_result();
    //vincular las variables de resultado
    $stmt->bind_result($old_msg, $fecha, $n_msgs_hoy);
    //obtenemos el valor
    $stmt->fetch();

    if($stmt->num_rows > 0){
        
      printf("Ya existe un usuario con ese nombre");
      header("Location: ../registro.php?error=nickExistente");
      exit;
      
    }
}else{
    printf("Error al buscar el usuario");
}

//buscamos si existe algun usuario con este email
if($stmt = $conn->prepare("SELECT * FROM Usuario WHERE email = ?")){
    $stmt->bind_param("s", $email);
    //ejecutamos la consulta
    $stmt->execute();
    
    $stmt->store_result();
    //vincular las variables de resultado
    $stmt->bind_result($nickRes, $passwordRes, $emailRes);
    //obtenemos el valor
    $stmt->fetch();

    if($stmt->num_rows > 0){
      printf("Ya existe un usuario con ese email");
      header("Location: ../registro.php?error=emailExistente");
      exit;
    }
}else{
    printf("Error al buscar el email");
}

//comprobamos contraseñas

if($pass != $pass2){
      printf("Las contraseñas no coinciden");
      header("Location: ../registro.php?error=passError");
}else{

    if (!($stmt = $conn->prepare("INSERT INTO Usuario VALUES (?,?,?)"))) {
	echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
    }

    if (!$stmt->bind_param("sss", $nick, $pass, $email)) {
	echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
    }

    if (!$stmt->execute()) {
	echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
    }
    session_start();
    $_SESSION["nick"] = $nickRes;
    header("Location: ../home.php");
    exit;
}
// Cerrar la conexión
$conn->close();
?>
