<?php

	include './BBDD.php';
	session_start();
	if($_SESSION["nick"] == null)
		header("Location: ../index.php?error=nCn");
	//definimos los parámetros del apartado anterior
	$etiquetas = $_POST["etiquetas"];
	$idTerminal = $_POST["idTerminal"];
	$aplicacion = $_POST["aplicacion"];
	$tipo = $_POST["tipo"];
	
	if($tipo == "app"){
		foreach ($etiquetas as $etiqueta) {
	        $BBDD = new BBDD(); 
	        $listaEtiquetas = $BBDD->almacenarEtiqueta($idTerminal,$aplicacion,$etiqueta);
	        $BBDD->terminarConexion();
	    }
	}else if($tipo == "mvl"){
		foreach ($etiquetas as $etiqueta) {
	        $BBDD = new BBDD(); 
	        $listaEtiquetas = $BBDD->almacenarEtiquetaTerminal($idTerminal,$etiqueta);
	        $BBDD->terminarConexion();
	    }	
	}else{
		print("Error, el tipo de la etiqueta no es correcto.");
	}
	header("Location:".$_SERVER['HTTP_REFERER']);
?>
