<?php
error_reporting(0);
$dbhost = "localhost";
$database="mydb";
$user="root";
$password="admin";

// Creacion de conexion
$conn = new mysqli($dbhost, $user, $password, $database);

// Comprobacion de conexion 
if ($conn->connect_error) {
    die("Error en la conexion a MySQL: " . $conn->connect_error);
}

//definimos los parámetros del apartado anterior --> CAMBIARLO POR JSON

$json = file_get_contents('php://input'); // ¿Recoge el JSON QUE LE LLEGA?
$login = json_decode($json); // aqui tenemos ya el json decodificado
$nick = $login->usr;
$pwd= $login->pwd;

//preparacion de la consulta
if($stmt = $conn->prepare("SELECT * FROM Usuario WHERE nick = ?")){
    $stmt->bind_param("s", $nick);
    //ejecutamos la consulta
    $stmt->execute();
    
    $stmt->store_result();
    //vincular las variables de resultado
    $stmt->bind_result($nickRes, $passwordRes, $emailRes);
    //obtenemos el valor
    $stmt->fetch();

    if($stmt->num_rows == 0 || $pwd != $passwordRes){
      //print("El usuario no existe o la contraseña es incorrecta");
      //print("La pass que envias es: %s y la que tengo es: %s",hash("sha256",pwd));
      print("fail");
    }else{    
      print("ok");
    }
    
}else{
    printf("Error al buscar el usuario");
}
// Cerrar la conexión
$conn->close();
?>
