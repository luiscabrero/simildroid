<?php   

    function eliminarAristasRepetidas($aristas){ //por alguna extraña razón, el array que contiene las aristas duplica las aristas (no he encontrado la solución) asi que he decidido crear esta funcion que elimina las aristas duplicadas.
	$aristasRes = array();    		  
	$ids = array();
	foreach($aristas as $a){
		if(!in_array($a->id, $ids)){ //si encontramos que el id ya está en el array, no lo insertamos.
	        	array_push($aristasRes,$a);
			array_push($ids, $a->id);
		}
	}
	return $aristasRes;
    }
    function bin2base64($bin) {//http://stackoverflow.com/questions/7607611/convert-a-binary-number-to-base-64
        $arr = str_split($bin, 8);
        $str = '';
        foreach ( $arr as $binNumber ) {
            $str .= chr(bindec($binNumber));
        }
        return base64_encode($str);
    }
    function base64bin($str) {//http://stackoverflow.com/questions/7607611/convert-a-binary-number-to-base-64
        $result = '';
        $str = base64_decode($str);
        $len = strlen($str);
        for ( $n = 0; $n < $len; $n++ ) {
            $result .= str_pad(decbin(ord($str[$n])), 8, '0', STR_PAD_LEFT);
        }
        return $result;
    }
    
    function permisos_String2B64($permisos){
        //asignamos valores, de manera que cada permiso asigna un bit distinto.
        $resArray = array();
        foreach($permisos as $p){

            switch ($p["Permiso_nombre"]) {
                case 'ACCESS_CHECKIN_PROPERTIES':           $resArray[144-138] = 1;break;
                case 'ACCESS_COARSE_LOCATION':              $resArray[144-137] = 1;break;
                case 'ACCESS_FINE_LOCATION':                $resArray[144-136] = 1;break;
                case 'ACCESS_LOCATION_EXTRA_COMMANDS':      $resArray[144-135] = 1;break;
                case 'ACCESS_NETWORK_STATE':                $resArray[144-134] = 1;break;
                case 'ACCESS_NOTIFICATION_POLICY':          $resArray[144-133] = 1;break;
                case 'ACCESS_WIFI_STATE':                   $resArray[144-132] = 1;break;
                case 'ACCESS_NOTIFICATION_POLICY':          $resArray[144-131] = 1;break;
                case 'ACCOUNT_MANAGER':                     $resArray[144-130] = 1;break;
                case 'ADD_VOICEMAIL':                       $resArray[144-129] = 1;break;
                case 'BATTERY_STATS':                       $resArray[144-128] = 1;break;
                case 'BIND_ACCESSIBILITY_SERVICE':          $resArray[144-127] = 1;break;
                case 'BIND_APPWIDGET':                      $resArray[144-126] = 1;break;
                case 'BIND_CARRIER_MESSAGING_SERVICE':      $resArray[144-125] = 1;break;
                case 'BIND_CARRIER_SERVICES':               $resArray[144-124] = 1;break;
                case 'BIND_CHOOSER_TARGET_SERVICE':         $resArray[144-123] = 1;break;
                case 'BIND_CONDITION_PROVIDER_SERVICE':     $resArray[144-122] = 1;break;
                case 'BIND_DEVICE_ADMIN':                   $resArray[144-121] = 1;break;
                case 'BIND_DREAM_SERVICE':                  $resArray[144-120] = 1;break;
                case 'BIND_INCALL_SERVICE':                 $resArray[144-119] = 1;break;
                case 'BIND_INPUT_METHOD':                   $resArray[144-118] = 1;break;
                case 'BIND_MIDI_DEVICE_SERVICE':            $resArray[144-117] = 1;break;
                case 'BIND_NFC_SERVICE':                    $resArray[144-116] = 1;break;
                case 'BIND_NOTIFICATION_LISTENER_SERVICE':  $resArray[144-115] = 1;break;
                case 'BIND_PRINT_SERVICE':                  $resArray[144-114] = 1;break;
                case 'BIND_QUICK_SETTINGS_TILE':            $resArray[144-113] = 1;break;
                case 'BIND_REMOTEVIEWS':                    $resArray[144-112] = 1;break;
                case 'BIND_SCREENING_SERVICE':              $resArray[144-111] = 1;break;
                case 'BIND_TELECOM_CONNECTION_SERVICE':     $resArray[144-110] = 1;break;
                case 'BIND_TEXT_SERVICE':                   $resArray[144-109] = 1;break;
                case 'BIND_TV_INPUT':                       $resArray[144-108] = 1;break;
                case 'BIND_VOICE_INTERACTION':              $resArray[144-107] = 1;break;
                case 'BIND_VPN_SERVICE':                    $resArray[144-106] = 1;break;
                case 'BIND_VR_LISTENER_SERVICE':            $resArray[144-105] = 1;break;
                case 'BIND_WALLPAPER':                      $resArray[144-104] = 1;break;
                case 'BLUETOOTH':                           $resArray[144-103] = 1;break;
                case 'BLUETOOTH_ADMIN':                     $resArray[144-102] = 1;break;
                case 'BLUETOOTH_PRIVILEGED':                $resArray[144-101] = 1;break;
                case 'BODY_SENSORS':                        $resArray[144-100] = 1;break;
                case 'BROADCAST_PACKAGE_REMOVED':           $resArray[144-99] = 1;break;
                case 'BROADCAST_SMS':                       $resArray[144-98] = 1;break;
                case 'BROADCAST_STICKY':                    $resArray[144-97] = 1;break;
                case 'BROADCAST_WAP_PUSH':                  $resArray[144-96] = 1;break;
                case 'CALL_PHONE':                          $resArray[144-95] = 1;break;
                case 'CALL_PRIVILEGED':                     $resArray[144-94] = 1;break;
                case 'CAMERA':                              $resArray[144-93] = 1;break;
                case 'CAPTURE_AUDIO_OUTPUT':                $resArray[144-92] = 1;break;
                case 'CAPTURE_SECURE_VIDEO_OUTPUT':         $resArray[144-91] = 1;break;
                case 'CAPTURE_VIDEO_OUTPUT':                $resArray[144-90] = 1;break;
                case 'CHANGE_COMPONENT_ENABLED_STATE':      $resArray[144-89] = 1;break;
                case 'CHANGE_CONFIGURATION':                $resArray[144-88] = 1;break;
                case 'CHANGE_NETWORK_STATE':                $resArray[144-87] = 1;break;
                case 'CHANGE_WIFI_MULTICAST_STATE':         $resArray[144-86] = 1;break;
                case 'CHANGE_WIFI_STATE':                   $resArray[144-85] = 1;break;
                case 'CLEAR_APP_CACHE':                     $resArray[144-84] = 1;break;
                case 'CONTROL_LOCATION_UPDATES':            $resArray[144-83] = 1;break;
                case 'DELETE_CACHE_FILES':                  $resArray[144-82] = 1;break;
                case 'DELETE_PACKAGES':                     $resArray[144-81] = 1;break;
                case 'DIAGNOSTIC':                          $resArray[144-80] = 1;break;
                case 'DISABLE_KEYGUARD':                    $resArray[144-79] = 1;break;
                case 'DUMP':                                $resArray[144-78] = 1;break;
                case 'EXPAND_STATUS_BAR':                   $resArray[144-77] = 1;break;
                case 'FACTORY_TEST':                        $resArray[144-76] = 1;break;
                case 'GET_ACCOUNTS':                        $resArray[144-75] = 1;break;
                case 'GET_ACCOUNTS_PRIVILEGED':             $resArray[144-74] = 1;break;
                case 'GET_PACKAGE_SIZE':                    $resArray[144-73] = 1;break;
                case 'GET_TASKS':                           $resArray[144-72] = 1;break;
                case 'GLOBAL_SEARCH';                       $resArray[144-71] = 1;break;
                case 'INSTALL_LOCATION_PROVIDER':           $resArray[144-70] = 1;break;
                case 'INSTALL_PACKAGES':                    $resArray[144-69] = 1;break;
                case 'INSTALL_SHORTCUT':                    $resArray[144-68] = 1;break;
                case 'INTERNET':                            $resArray[144-67] = 1;break;
                case 'KILL_BACKGROUND_PROCESSES':           $resArray[144-66] = 1;break;
                case 'LOCATION_HARDWARE':                   $resArray[144-65] = 1;break;
                case 'MANAGE_DOCUMENTS':                    $resArray[144-64] = 1;break;
                case 'MASTER_CLEAR':                        $resArray[144-63] = 1;break;
                case 'MEDIA_CONTENT_CONTROL':               $resArray[144-62] = 1;break;
                case 'MODIFY_AUDIO_SETTINGS':               $resArray[144-61] = 1;break;
                case 'MODIFY_PHONE_STATE':                  $resArray[144-60] = 1;break;
                case 'MOUNT_FORMAT_FILESYSTEMS':            $resArray[144-59] = 1;break;
                case 'MOUNT_UNMOUNT_FILESYSTEMS':           $resArray[144-58] = 1;break;
                case 'NFC':                                 $resArray[144-57] = 1;break;
                case 'PACKAGE_USAGE_STATS':                 $resArray[144-56] = 1;break;
                case 'PERSISTENT_ACTIVITY':                 $resArray[144-55] = 1;break;
                case 'PROCESS_OUTGOING_CALLS':              $resArray[144-54] = 1;break;
                case 'READ_CALENDAR':                       $resArray[144-53] = 1;break;
                case 'READ_CALL_LOG':                       $resArray[144-52] = 1;break;
                case 'READ_CONTACTS':                       $resArray[144-51] = 1;break;
                case 'READ_EXTERNAL_STORAGE':               $resArray[144-50] = 1;break;
                case 'READ_FRAME_BUFFER':                   $resArray[144-49] = 1;break;
                case 'READ_INPUT_STATE':                    $resArray[144-48] = 1;break;
                case 'READ_LOGS':                           $resArray[144-47] = 1;break;
                case 'READ_PHONE_STATE':                    $resArray[144-46] = 1;break;
                case 'READ_SMS':                            $resArray[144-45] = 1;break;
                case 'READ_SYNC_SETTINGS':                  $resArray[144-44] = 1;break;
                case 'READ_SYNC_STATS':                     $resArray[144-43] = 1;break;
                case 'READ_VOICEMAIL':                      $resArray[144-42] = 1;break;
                case 'REBOOT':                              $resArray[144-41] = 1;break;
                case 'RECEIVE_BOOT_COMPLETED':              $resArray[144-40] = 1;break;
                case 'RECEIVE_MMS':                         $resArray[144-39] = 1;break;
                case 'RECEIVE_SMS':                         $resArray[144-38] = 1;break;
                case 'RECEIVE_WAP_PUSH':                    $resArray[144-37] = 1;break;
                case 'RECORD_AUDIO':                        $resArray[144-36] = 1;break;
                case 'REORDER_TASKS':                       $resArray[144-35] = 1;break;
                case 'REQUEST_IGNORE_BATTERY_OPTIMIZATIONS':$resArray[144-34] = 1;break;
                case 'REQUEST_INSTALL_PACKAGES':            $resArray[144-33] = 1;break;
                case 'RESTART_PACKAGES':                    $resArray[144-32] = 1;break;
                case 'SEND_RESPOND_VIA_MESSAGE':            $resArray[144-31] = 1;break;
                case 'SEND_SMS':                            $resArray[144-30] = 1;break;
                case 'SET_ALARM':                           $resArray[144-29] = 1;break;
                case 'SET_ALWAYS_FINISH':                   $resArray[144-28] = 1;break;
                case 'SET_ANIMATION_SCALE':                 $resArray[144-27] = 1;break;
                case 'SET_DEBUG_APP':                       $resArray[144-26] = 1;break;
                case 'SET_PREFERRED_APPLICATIONS':          $resArray[144-25] = 1;break;
                case 'SET_PROCESS_LIMIT':                   $resArray[144-24] = 1;break;
                case 'SET_TIME':                            $resArray[144-23] = 1;break;
                case 'SET_TIME_ZONE':                       $resArray[144-22] = 1;break;
                case 'SET_WALLPAPER':                       $resArray[144-21] = 1;break;
                case 'SET_WALLPAPER_HINTS':                 $resArray[144-20] = 1;break;
                case 'SIGNAL_PERSISTENT_PROCESSES':         $resArray[144-19] = 1;break;
                case 'STATUS_BAR':                          $resArray[144-18] = 1;break;
                case 'SYSTEM_ALERT_WINDOW':                 $resArray[144-17] = 1;break;
                case 'TRANSMIT_IR':                         $resArray[144-16] = 1;break;
                case 'UNINSTALL_SHORTCUT':                  $resArray[144-15] = 1;break;
                case 'UPDATE_DEVICE_STATS':                 $resArray[144-14] = 1;break;
                case 'USE_FINGERPRINT':                     $resArray[144-13] = 1;break;
                case 'USE_SIP':                             $resArray[144-12] = 1;break;
                case 'VIBRATE':                             $resArray[144-11] = 1;break;
                case 'WAKE_LOCK':                           $resArray[144-10] = 1;break;
                case 'WRITE_APN_SETTINGS':                  $resArray[144-9] = 1;break;
                case 'WRITE_CALENDAR':                      $resArray[144-8] = 1;break;
                case 'WRITE_CALL_LOG':                      $resArray[144-7] = 1;break;
                case 'WRITE_CONTACTS':                      $resArray[144-6] = 1;break;
                case 'WRITE_EXTERNAL_STORAGE':              $resArray[144-5] = 1;break;
                case 'WRITE_GSERVICES':                     $resArray[144-4] = 1;break;
                case 'WRITE_SECURE_SETTINGS':               $resArray[144-3] = 1;break;
                case 'WRITE_SYNC_SETTINGS':                 $resArray[144-2] = 1;break;
                case 'WRITE_SETTINGS':                      $resArray[144-1] = 1;break;
                default://print "El permiso ".$p["Permiso_nombre"]." no se tiene en cuenta. <br> ";break;
            }
        }
        $res = 0;
        //unificamos para encontrar el array de bit completo
        for($i = 0; $i < 144; $i++){
            if($resArray[$i] === 1)
                $res = $res."1";
            else {
                $res = $res."0";
            }
        }
        return bin2base64($res);
    }

    
    
	session_start();
	if($_SESSION["nick"] == null)
		header("Location: ../index.php?error=nCn");
		
    //include './BBDD.php'; //incluimos este fichero para poder hacer llamadas a la base de datos.
    include './graphmlObjects.php'; //incluimos este fichero para poder crear objetos de tipo Nodo y Arista.
    
    $hoy = getdate(); //obtenemos fecha para crear el fichero personalizado
    $file = fopen("./tmp/".$nombreGrafoEstructural, "w"); //abrimos el fichero donde vamos a generar el contenido del GraphML

    //generamos la parte común del fichero, todos los GML tendrán estas primeras líneas comunes.
    fwrite($file, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
    <graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\"
    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
    xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns
    http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\">

    <key id=\"v_name\" for=\"node\" attr.name=\"name\" attr.type=\"string\"/>
    <key id=\"v_nt\" for=\"node\" attr.name=\"nt\" attr.type=\"string\"/>
    <key id=\"v_myid\" for=\"node\" attr.name=\"myid\" attr.type=\"string\"/>
    <key id=\"v_ft\" for=\"node\" attr.name=\"ft\" attr.type=\"string\"/>
    <key id=\"v_tt\" for=\"node\" attr.name=\"tt\" attr.type=\"string\"/>
    <key id=\"v_st\" for=\"node\" attr.name=\"st\" attr.type=\"string\"/>
    <key id=\"v_id\" for=\"node\" attr.name=\"id\" attr.type=\"string\"/>
    <graph id=\"G\" edgedefault=\"undirected\">".PHP_EOL); 
        
    /*
    * Inicializamos estructuras de datos necesarias para el correcto funcionamiento del código
    */
	$nNodo = array();
	$nodos = array();
	$aristas = array();
	$nArista = 0;    
	$nAplicacion = 0;
	$nodoRaiz = 0;
	$categorias = array(); //array que va a contener las categorias de las apps
	$autores = array(); //array que va a contener los autores de las apps
	$nombres = array(); //array que va a contener los nombres de las apps
	$permisos = array(); //array que va a contener los permisos de las apps


	$aristah = 0;
    /* Primero generamos los objetos tipo Nodo y tipo Arista, para después escribirlos ordenados */

    //Primer Paso, obtener todos los terminales del usuario --> generar un nodo (tipo dev)
	$BBDD = new BBDD(); 
	$terminales = $BBDD->obtenerTerminales($_SESSION["nick"]);
	$BBDD->terminarConexion();

    if($terminales != null){ //caso imposible??
        // ANTES DE NADA, CREAMOS EL NODO QUE VA A UNIR EL GRAFO POR COMPLETO. (nodo raiz)
        $nuevoNodo = new Nodo();
        $nuevoNodo->v_nt = "str"; 
        $nuevoNodo->v_myid = "HUB";//nodo de tipo "HUB"
        $nuevoNodo->v_ft = "NS";
        $nuevoNodo->v_tt = "NS";
        $nuevoNodo->v_st = "dev";
        $nuevoNodo->v_id = "n".sizeof($nNodo);
        $nuevoNodo->v_name = "HUB";
        array_push($nodos,$nuevoNodo);
        array_push($nNodo,$nuevoNodo->v_name);
        
        $nodoRaiz = $nuevoNodo->v_id;
        
        // CREAMOS EL NODO DE TIPO DE DEVICE Y EL DE TIPO WEARABLE (luego los conectaremos con los respectivos terminales.)
        
	
	//nodo tipo mobile_phone
        
        $nuevoNodo = new Nodo();
        $nuevoNodo->v_nt = "fea"; 
        $nuevoNodo->v_myid = "mobile phone";//nodo de tipo "mobile phone"
        $nuevoNodo->v_ft = "dev_type";
        $nuevoNodo->v_tt = "NS";
        $nuevoNodo->v_st = "NS";
        $nuevoNodo->v_id = "n".sizeof($nNodo);
        $nuevoNodo->v_name = "mobile phone";
        
        $numeroNodoMP = "n".sizeof($nNodo); //para asociar el nodo Mobile Phone con los terminales de dicho tipo.
        
        array_push($nodos,$nuevoNodo); //insertamos el nodo en el conjunto de nodos
        array_push($nNodo,$nuevoNodo->v_name); //agregamos el nombred del nodo a la lista de nombres de nodo
        
        //nodo tipo wearable
        $nuevoNodo = new Nodo();
        $nuevoNodo->v_nt = "fea";
        $nuevoNodo->v_myid = "wearable"; //nodo de tipo "wearable"
        $nuevoNodo->v_ft = "dev_type";
        $nuevoNodo->v_tt = "NS";
        $nuevoNodo->v_st = "NS";
        $nuevoNodo->v_id = "n".sizeof($nNodo);
        $nuevoNodo->v_name = "wearable";
        
        $numeroNodoW = "n".sizeof($nNodo);  //para luego asociar el nodo de tipo Wearable con los nodos wearables.
        
        array_push($nodos,$nuevoNodo); //insertamos el nodo en el conjunto de nodos
        array_push($nNodo,$nuevoNodo->v_name); //agregamos el nombred del nodo a la lista de nombres de nodo
            
        foreach($terminales as $terminal){ //por cada terminal, generamos un nodo.
        
            $nuevoNodo = new Nodo();
            $nuevoNodo->v_nt = "str"; //nodo de tipo Terminal
            $nuevoNodo->v_myid = "d".$terminal["idTerminal"];
            $nuevoNodo->v_ft = "NS";
            $nuevoNodo->v_tt = "NS";
            $nuevoNodo->v_st = "dev";
            $nuevoNodo->v_id = "n".sizeof($nNodo);
            $nuevoNodo->v_name = "d".$terminal["idTerminal"];
                
            array_push($nodos,$nuevoNodo); //insertamos el nodo en el conjunto de nodos
            array_push($nNodo,$nuevoNodo->v_name); //agregamos el nombred del nodo a la lista de nombres de nodo
            
            
            //ahora asociamos los tipos de dev (en caso de ser Mobile Phone, habrá que crear además, una arista al nodo raiz)
            if($terminal["tipo"] === "movil"){
                //crear arista que una con el tipo Mobile Phone
                $nuevaAristaA = new Arista();
                $nuevaAristaA->nodoOrigen = $nuevoNodo->v_id; //nodo terminal 
                $nuevaAristaA->nodoDestino = $numeroNodoMP; //nodo app
                $nuevaAristaA->id = $aristah++; 
                array_push($aristas,$nuevaAristaA);
                $nArista++; //Una nueva arista
                //crear arista que una con nodo raiz
                                
                $nuevaAristaA = new Arista();
                $nuevaAristaA->nodoOrigen = $nuevoNodo->v_id; //nodo terminal 
                $nuevaAristaA->nodoDestino = $nodoRaiz; //nodo app
                $nuevaAristaA->id = $aristah++; 
                array_push($aristas,$nuevaAristaA);
                $nArista++; //Una nueva arista
                
            }else if($terminal["tipo"] === "wearable"){
                //crear arista que una con el tipo Wearable
                                
                $nuevaAristaA = new Arista();
                $nuevaAristaA->nodoOrigen = $nuevoNodo->v_id; //nodo terminal 
                $nuevaAristaA->nodoDestino = $numeroNodoW; //nodo app
                $nuevaAristaA->id = $aristah++; 
                array_push($aristas,$nuevaAristaA);
                $nArista++; //Una nueva arista
            }else{
                //print "Existe un error al determinar el tipo de dispositivo";
            }
            
            
            //Acceder, para cada terminal, a todas las aplicaciones instaladas --> generar por cada app, un nodo, conectándolo al nodo del terminal (edges).
            $BBDD = new BBDD();
            $aplicaciones = $BBDD->obtenerAplicaciones($terminal["idTerminal"]); //obtenemos apps de este terminal
            $BBDD->terminarConexion();
                    
            
            
            
            /**
             *   A   P   L   I   C   A   C   I   O   N   E   S 
             */
            
            if($aplicaciones != null)
            foreach($aplicaciones as $aplicacion){ //por cada aplicacion generamos un nodo y una arista que conecta el terminal con la aplicacion.
                        
                $nuevoNodoA = new Nodo();
                $nuevoNodoA->v_nt = "str"; //nodo de tipo Terminal
                $nuevoNodoA->v_myid = $aplicacion["nombre"]."-".$aplicacion["Terminal_idTerminal"];
                $nuevoNodoA->v_ft = "NS";
                $nuevoNodoA->v_tt = "NS";
                $nuevoNodoA->v_st = "app";
                $nuevoNodoA->v_id = "n".sizeof($nNodo);
                $nuevoNodoA->v_name = $aplicacion["nombre"]."-".$aplicacion["Terminal_idTerminal"];
                
                array_push($nodos,$nuevoNodoA);
                array_push($nNodo,$nuevoNodoA->v_name);
                
                $nuevaAristaA = new Arista();
                $nuevaAristaA->nodoOrigen = $nuevoNodo->v_id; //nodo terminal 
                $nuevaAristaA->nodoDestino = $nuevoNodoA->v_id; //nodo app
                $nuevaAristaA->id = $aristah++; 
                
                array_push($aristas,$nuevaAristaA);
                $nArista++; //Una nueva arista
                
                
                //tenemos que buscar todas las etiquetas de una app, y crear los nodos correspondientes (y las aristas)
                $BBDD = new BBDD();
                $etiquetas = $BBDD->obtenerEtiquetas($terminal["idTerminal"],$aplicacion["nombre"]);//obtenemos etiquetas de esta app
                $BBDD->terminarConexion();
                
                if($etiquetas != null)
                foreach($etiquetas as $etiqueta){//por cada etiqueta generamos un nodo y una arista que conecta la app con la etiqueta.
                    
                    //antes de crear un nodo, tenemos que comprobar si existe esa etiqueta ya, en cuyo caso, únicamente habría que crear la conexion (arista)
                    
                    $existeNodoE = array_search("t:".$etiqueta["Etiqueta_nombre"], $nNodo);
                    
                    if($existeNodoE === FALSE){ //el nodo no existe, lo creamos.
                        
                        $nuevoNodoE = new Nodo();
                        $nuevoNodoE->v_nt = "tag"; //nodo de tipo Terminal
                        $nuevoNodoE->v_myid = "t:".$etiqueta["Etiqueta_nombre"];
                        $nuevoNodoE->v_ft = "NS";
                        $nuevoNodoE->v_tt = $etiqueta["Etiqueta_nombre"]; // aquí hay que meter la asociación de etiqueta con la etiqueta concreta del usuario.
                        $nuevoNodoE->v_st = "NS";
                        $nuevoNodoE->v_id = "n".sizeof($nNodo);
                        $nuevoNodoE->v_name = "t:".$etiqueta["Etiqueta_nombre"];
            
                        array_push($nodos,$nuevoNodoE);
                        array_push($nNodo,$nuevoNodoE->v_name);   
    
                        $nuevaAristaE = new Arista();
                        $nuevaAristaE->nodoOrigen = $nuevoNodoA->v_id; //nodo app 
                        $nuevaAristaE->nodoDestino = $nuevoNodoE->v_id; //nodo etiqueta
                	$nuevaAristaE->id = $aristah++; 

                    
                        array_push($aristas,$nuevaAristaE);
                        $nArista++; //Una nueva arista
                    
                    }else{ //el nodo de ese tag ya existe, por lo que ahora tendremos que conectar la aplicación con ese tag. (solo arista)
                        
                        $nuevaAristaE = new Arista();
                        $nuevaAristaE->nodoOrigen = $nuevoNodoA->v_id; //nodo app 
                        $nuevaAristaE->nodoDestino = "n".$existeNodoE; //nodo etiqueta -->arraySearch nos devuelve el número de nodo, que debería coincidir. (OJO!!!!!!!!!!!!!!)
	                $nuevaAristaE->id = $aristah++; 
                    
                        array_push($aristas,$nuevaAristaE);
                        $nArista++; //Una nueva arista
                    }
    
                    
                }
                
                //tenemos que buscar la categoría de la app, y crear el nodo correspondiente (si no exista ya)
                $BBDD = new BBDD();
                $categoria = $BBDD->obtenerCategoria($aplicacion["paquete"]);//obtenemos etiquetas de esta app
                $BBDD->terminarConexion();
                
                foreach($categoria as $cat)
                    $categoria = $cat["nombre"];
                
                $existeCategoria = array_search($categoria, $categorias);
                
                if($existeCategoria === FALSE){ //crear nodo y arista
                    $nuevoNodoC = new Nodo(); //generamos el nodo
                    $nuevoNodoC->v_nt = "fea"; 
                    $nuevoNodoC->v_myid = $categoria;
                    $nuevoNodoC->v_ft = "app_category";
                    $nuevoNodoC->v_tt = "NS"; 
                    $nuevoNodoC->v_st = "NS";
                    $nuevoNodoC->v_id = "n".sizeof($nNodo);
                    $nuevoNodoC->v_name = $categoria;
            
                    array_push($nodos,$nuevoNodoC);
                    array_push($nNodo,$nuevoNodoC->v_name);   
    
                    $nuevaAristaC = new Arista();
                    $nuevaAristaC->nodoOrigen = $nuevoNodoA->v_id; //nodo app 
                    $nuevaAristaC->nodoDestino = $nuevoNodoC->v_id; //nodo categoria
		    $nuevaAristaC->id = $aristah++; 
                
                    array_push($aristas,$nuevaAristaC);
                    $nArista++; //Una nueva arista
                    array_push($categorias,$categoria);
    
                }else{
                    $nodoCategoriaExistente = -1;
                    foreach($nodos as $n){
                        if($n->v_ft === "app_category" && $n->v_name === $categoria)
                            $nodoCategoriaExistente = $n->v_id;   
                    }
                    
                    $nuevaAristaC = new Arista();
                    $nuevaAristaC->nodoOrigen = $nuevoNodoA->v_id; //nodo app 
                    $nuevaAristaC->nodoDestino = $nodoCategoriaExistente; //nodo categoria  HAY QUE BUSCAR EL NODO DE LA CATEGORIA!!!!!!
       		    $nuevaAristaC->id = $aristah++; 
                    
		    array_push($aristas,$nuevaAristaC);
                    $nArista++; //Una nueva arista
                    array_push($categorias,$categoria);
                }
                
                //tenemos que buscar el autor de la app, y crear el nodo correspondiente (si no exista ya)
                $autor = $aplicacion["autor"];
                
                $existeAutor = array_search($autor, $autores);
                
                if($existeAutor === FALSE){ //crear nodo y arista
                    //print("NO EXISTE EL AUTOR: ".$autor.", lo creo.<br>");
                    $nuevoNodoAA = new Nodo(); //generamos el nodo
                    $nuevoNodoAA->v_nt = "fea"; 
                    $nuevoNodoAA->v_myid =$autor."_author";
                    $nuevoNodoAA->v_ft = "app_author";
                    $nuevoNodoAA->v_tt = "NS"; 
                    $nuevoNodoAA->v_st = "NS";
                    $nuevoNodoAA->v_id = "n".sizeof($nNodo);
                    //print("Autor metido con id de nodo = ".$nuevoNodoAA->v_id."<br>");
                    $nuevoNodoAA->v_name =$autor."_author";
            
                    array_push($nodos,$nuevoNodoAA);
                    array_push($nNodo,$nuevoNodoAA->v_name);   
    
                    $nuevaAristaAA = new Arista();
                    $nuevaAristaAA->nodoDestino = $nuevoNodoA->v_id; //nodo app 
                    $nuevaAristaAA->nodoOrigen = $nuevoNodoAA->v_id; //nodo categoria
                    $nuevaAristaAA->id = $aristah++; 
                    array_push($aristas,$nuevaAristaAA);
                    $nArista++; //Una nueva arista
                    array_push($autores,$autor);
    
                }else{
                    //print("SI EXISTE EL AUTOR: ".$autor.", lo junto.<br>");

                    //print("Si xiste<br>");
                    $nodoAutorExistente = -1;
                    foreach($nodos as $n){
                        //print($n->v_name." con ". $autor);
                        if($n->v_ft === "app_author" && $n->v_name === $autor){
                            $nodoAutorExistente = $n->v_id; 
                            
                            $nuevaAristaAA = new Arista();
                            $nuevaAristaAA->nodoOrigen = $nodoAutorExistente; //nodo categoria  HAY QUE BUSCAR EL NODO DE LA CATEGORIA!!!!!!
                            $nuevaAristaAA->nodoDestino = $nuevoNodoA->v_id; //nodo app 
                            $nuevaAristaAA->id = $aristah++; 
                            array_push($aristas,$nuevaAristaAA);
                            //print("HE A&Ntilde;ADIDO LA ARISTA ENTRE ".$autor." y ". $nuevoNodoA->v_id."<br>");
                            $nArista++; //Una nueva arista
                            array_push($autores,$autor);
                        }
                    }
                }
                
                //tenemos que buscar el nombre de la app, y crear el nodo correspondiente (si no exista ya)
                $nombre = $aplicacion["nombre"];
                
                $existeNombre = array_search($nombre, $nombres);
                
                if($existeNombre === FALSE){ //crear nodo y arista
                    $nuevoNodoNA = new Nodo(); //generamos el nodo
                    $nuevoNodoNA->v_nt = "fea"; 
                    $nuevoNodoNA->v_myid = $nombre;
                    $nuevoNodoNA->v_ft = "app_name";
                    $nuevoNodoNA->v_tt = "NS"; 
                    $nuevoNodoNA->v_st = "NS";
                    $nuevoNodoNA->v_id = "n".sizeof($nNodo);
                    //print("Autor metido con id de nodo = ".$nuevoNodoAA->v_id."<br>");
                    $nuevoNodoNA->v_name = $nombre;
            
                    array_push($nodos,$nuevoNodoNA);
                    array_push($nNodo,$nuevoNodoNA->v_name);   
    
                    $nuevaAristaNA = new Arista();
                    $nuevaAristaNA->nodoDestino = $nuevoNodoA->v_id; //nodo app 
                    $nuevaAristaNA->nodoOrigen = $nuevoNodoNA->v_id; //nodo categoria
                    $nuevaAristaNA->id = $aristah++; 
                    
                    array_push($aristas,$nuevaAristaNA);
                    $nArista++; //Una nueva arista
                    array_push($nombres,$nombre);
    
                }else{
		    //print("<b>SI</b> existe el nombre de la app: ".$nombre."<br>");
                    $nodoNombreExistente = -1;
                    foreach($nodos as $n){
                        if($n->v_ft === "app_name" && $n->v_name === $nombre){
                            $nodoNombreExistente = $n->v_id; 
                            //print "Encontrado en los apps_name el siguiente app_name: ".$n->v_name."<br>";
                            $nuevaAristaNA = new Arista();
                            $nuevaAristaNA->nodoOrigen = $nodoNombreExistente; //nodo categoria  HAY QUE BUSCAR EL NODO DE LA CATEGORIA!!!!!!
                            $nuevaAristaNA->nodoDestino = $nuevoNodoA->v_id; //nodo app 
                            $nuevaAristaNA->id = $aristah++; 
                            array_push($aristas,$nuevaAristaNA);
                            $nArista++; //Una nueva arista
                            array_push($nombres,$nombre);
                        }
                    }
                }
                
                //tenemos que buscar los permisos de la app, y crear el nodo correspondiente (si no exista ya)
                $BBDD = new BBDD();
                $permisosLista = $BBDD->obtenerPermisos($aplicacion["Terminal_idTerminal"],$aplicacion["nombre"]);//obtenemos etiquetas de esta app
                $BBDD->terminarConexion();
                
                $permisos_B64 = permisos_String2B64($permisosLista); //obtenemos la lista de permisos para la app en b64.
                //print("<br>".$permisos_B64."<br>");
                                
                $existePermiso = array_search($permisos_B64, $permisos);
                if($existePermiso === FALSE){ //crear nodo y arista

                    $nuevoNodoP = new Nodo(); //generamos el nodo
                    $nuevoNodoP->v_nt = "fea"; 
                    $nuevoNodoP->v_myid = $permisos_B64;
                    $nuevoNodoP->v_ft = "app_permissionset";
                    $nuevoNodoP->v_tt = "NS"; 
                    $nuevoNodoP->v_st = "NS";
                    $nuevoNodoP->v_id = "n".sizeof($nNodo);
                    $nuevoNodoP->v_name = $permisos_B64;
            
                    array_push($nodos,$nuevoNodoP);
                    array_push($nNodo,$nuevoNodoP->v_name);   
    			 //print("<br>---------@# El permiso (<b>NO</b> existe): ".$nuevoNodoP->v_id." est&aacute; asociada a la app: ".$nuevoNodoA->v_id." #@---------<br>");
                    $nuevaAristaP = new Arista();
                    $nuevaAristaP->nodoDestino = $nuevoNodoA->v_id; //nodo app 
                    $nuevaAristaP->nodoOrigen = $nuevoNodoP->v_id; //nodo permiso
                    $nuevaAristaP->id = $aristah++; 
                    array_push($aristas,$nuevaAristaP);
                    $nArista++; //Una nueva arista
                    array_push($permisos,$permisos_B64);
    
                }else{
                    $nodoPermisoExistente = -1;
                    foreach($nodos as $n){
                        if($n->v_ft === "app_permissionset" && $n->v_name === $permisos_B64){
                            $nodoPermisoExistente = $n->v_id; 
                            $nuevaAristaP = new Arista();
                            $nuevaAristaP->nodoOrigen = $nodoPermisoExistente; //nodo categoria
                            $nuevaAristaP->nodoDestino = $nuevoNodoA->v_id; //nodo app 
                            $nuevaAristaP->id = $aristah++; 
                            array_push($aristas,$nuevaAristaNA);
                            $nArista++; //Una nueva arista
                            array_push($permisos,$permisos_B64);
                        }
                    }
		    //print("<br>---------@# El permiso (<b>SI</b> existe): ".$nodoPermisoExistente." est&aacute; asociada a la app: ".$nuevoNodoA->v_id." #@---------<br>");
                }
                
            }
            
        }
        //ahora miramos lo de los wearable.
        
        //buscamos los nodos que son devices
        $nodosDevices = array();
        foreach($nodos as $n){
            if($n->v_st === "dev")
                array_push($nodosDevices,$n->v_id);
        }

        foreach($terminales as $terminal){
            if($terminal["tipo"] === "wearable"){ //si se trata de un dispositivo wearable...
                //conectamos dicho nodo con el nodo de tipo Wearable (feature)
                
                $appsAsociadas = json_decode($terminal["appAsociada"]);
		$NodoT = 0;
                foreach ($nodos as $n) {//buscamos el id del nodo del wearable (nodo estructural) para conectarlo con el nodo de tipo Wearable (nodo atributo).
                        if($n->v_st === "dev"){
                            if("d".$terminal["idTerminal"] === $n->v_name){
                                $NodoT = $n->v_id;
				//print ("Este es el dev con nombre: ".$n->v_name."<br>");
                            }
                        }
                        
                }
                foreach($appsAsociadas as $appAsociada){// por cada app asociada, vamos a crear un nodo, y una conexión a la app del terminal asociado
                //En primer lugar, generamos el nodo de la app Asociada
                    $t = substr($appAsociada, strripos($appAsociada, "-")+1); //obtenemos el id del terminal almacenado en el nombre de la app

                    $nuevoNodoA = new Nodo();
                    $nuevoNodoA->v_nt = "str"; //nodo de tipo Terminal
                    $nuevoNodoA->v_myid = substr($appAsociada,0,strripos($appAsociada, "-"))."-".$terminal["idTerminal"];
                    $nuevoNodoA->v_ft = "NS";
                    $nuevoNodoA->v_tt = "NS";
                    $nuevoNodoA->v_st = "app";
                    $nuevoNodoA->v_id = "n".sizeof($nNodo);
                    $nuevoNodoA->v_name = substr($appAsociada,0,strripos($appAsociada, "-"))."-".$terminal["idTerminal"];
                    
                    array_push($nodos,$nuevoNodoA);
                    array_push($nNodo,$nuevoNodoA->v_name);
                    //print "juntamos el device: ".$NodoT. " con la app: ".$nuevoNodoA->v_name."<br>";
                    // lo unimos al terminal wearable
                    $nuevaAristaA = new Arista();
                    $nuevaAristaA->nodoOrigen = $NodoT; //nodo terminal 
                    $nuevaAristaA->nodoDestino = $nuevoNodoA->v_id; //nodo app
                    $nuevaAristaA->id = $aristah++; 
                    array_push($aristas,$nuevaAristaA);
                    $nArista++; //Una nueva arista
                
                    foreach ($nodos as $n) { //buscamos el nodo original (la app original de la que copiamos toda la información)
                        if($n->v_st === "app"){
                            if($appAsociada === $n->v_name)
                                $NodoAppOriginal = $n->v_id;
                                //print("la app ".$appAsociada." es la misma que: ".$n->v_name."<br>");
                        }
                    }
                    
                    //tenemos que crear una conexion entre el terminal asociado y el wearable !! (SOLO SI NO EXISTE YA LA CONEXION)
                    //primero tenemos que buscar el numero del nodo (idNodo) asociado al terminal asociado.
                    foreach ($nodos as $n) {
                        if($n->v_st === "dev" && $n->v_name ==="d".$t){ //si el nodo es de tipo dev y coincide con el terminal asociado --> hemos encontrado el nodo del terminal
                            $idNodoTerminalAsociado = $n->v_id;
                        }
                    }
                    
                    $existeAristaTerminal2Wearable = FALSE;
                    //print("ANTES DE NADA, el nodo del terminal asociado es: ".$t." y el nodo del terminal wearable es: ".$NodoT);
                    foreach ($aristas as $ar) {
                    //print("PUES ESTAMOS COMPROBANDO SI: ".$ar->nodoDestino." es lo mismo que: ".$idNodoTerminalAsociado." y si ".$ar->nodoOrigen." es lo mismo que: ".$NodoT."<br>");
                    //print(" Y ADEMAS SI: ".$ar->nodoDestino." es lo mismo que: ".$NodoT." y si ".$ar->nodoOrigen." es lo mismo que: ".$idNodoTerminalAsociado."<br>");
                        if(($ar->nodoDestino === $idNodoTerminalAsociado && $ar->nodoOrigen === $NodoT) || ($ar->nodoDestino === $NodoT && $ar->nodoOrigen === $idNodoTerminalAsociado )){
                            $existeAristaTerminal2Wearable = TRUE;
                            //print(" YA EXISTE jijijij <br>");
                        }
                    }
                    if(!$existeAristaTerminal2Wearable){ //si no existe la arista que conecta los dos terminales, lo creamos
                        $nuevaAristaTW = new Arista();
                        $nuevaAristaTW->nodoOrigen = $idNodoTerminalAsociado; //nodo terminal 
                        $nuevaAristaTW->nodoDestino = $NodoT; //nodo app
			$nuevaAristaTW->id = $aristah++; 
                        array_push($aristas,$nuevaAristaTW);
                        $nArista++; //Una nueva arista    
                    }
                    
                    
                    
                    
                    //al crear un nodo "copia" de la app asociada, también tendremos que generar aristas a las respectivas categorías /autores / etc etc
                    //para generar estas aristas, necesitaremos saber el id de la app de la que queremos copiar la info ($NodoAppOriginal) y por otra parte, 
                    // el id del nodo de los terminales, ya que no queremos conectarlo a ningún terminal que no sea el wearable.
                    
                    foreach($aristas as $a){//buscamos en las aristas el nodo del que queremos copiar la info, y comprobamos que en esa union, no hay ningún device.
                        if($a->nodoOrigen === $NodoAppOriginal && array_search($a->nodoDestino,$nodosDevices) === FALSE && array_search($a->nodoOrigen,$nodosDevices) === FALSE){ 
                            $nuevaAristaA = new Arista();
                            $nuevaAristaA->nodoOrigen = $a->nodoDestino; //nodo terminal 
                            $nuevaAristaA->nodoDestino = $nuevoNodoA->v_id; //nodo app
			    $nuevaAristaA->id = $aristah++; 
                            array_push($aristas,$nuevaAristaA);
                            $nArista++; //Una nueva arista
                        }else if($a->nodoDestino === $NodoAppOriginal && array_search($a->nodoDestino,$nodosDevices) === FALSE && array_search($a->nodoOrigen,$nodosDevices) === FALSE){ 
                            $nuevaAristaA = new Arista();
                            $nuevaAristaA->nodoOrigen = $a->nodoOrigen; //nodo terminal 
                            $nuevaAristaA->nodoDestino = $nuevoNodoA->v_id; //nodo app
                	    $nuevaAristaA->id = $aristah++; 
                            array_push($aristas,$nuevaAristaA);
                            $nArista++; //Una nueva arista
                        }
                    }

                    $nuevaAristaA = new Arista();
                    $nuevaAristaA->nodoOrigen = $NodoAppOriginal; //nodo terminal 
                    $nuevaAristaA->nodoDestino = $nuevoNodoA->v_id; //nodo app
                    $nuevaAristaA->id = $aristah++; 
                    array_push($aristas,$nuevaAristaA);
                    $nArista++; //Una nueva arista
                    
                }
            }else{
                //conectamos el terminal con el tipo mobile phone
            }  
            
        }
    }
    print (" --------------->> GRAFO ESTRUCTURAL <<--------------- ");
    print(" # # # # # # # # # # # # # #  N  O  D  O  S  # # # # # # # # # # # # # # <br>");

    foreach($nodos as $nodo){
        print("Existe un nodo con id: ".$nodo->v_id." y nombre: ".str_replace('&', 'y', $nodo->v_name)." <br>");
    }
    
    $aristasSinRepetir = eliminarAristasRepetidas($aristas);
    print(" # # # # # # # # # # # # # #  A   R   I   S   T   A   S  2.0# # # # # # # # # # # # # # ".sizeof($aristasSinRepetir)."<br>");

    foreach($aristasSinRepetir as $arista){
        print("Existe una arista que va desde ".$arista->nodoOrigen." hasta ".$arista->nodoDestino." y tiene id= ".$arista->id."<br>");
    }
    
    //tras haber creado todos los nodos y las aristas, ahora debemos escribirlos en el .graphml
    
    /* E S C R I T U R A  D E  N O D O S */
    
    foreach ($nodos as $nodo) { 
        fwrite($file, "
        <node id=\"".$nodo->v_id."\">
            <data key=\"v_nt\">".$nodo->v_nt."</data>
            <data key=\"v_myid\">".str_replace('&', 'y', $nodo->v_myid)."</data>
            <data key=\"v_ft\">".$nodo->v_ft."</data>
            <data key=\"v_tt\">".$nodo->v_tt."</data>
            <data key=\"v_st\">".$nodo->v_st."</data>
            <data key=\"v_id\">".$nodo->v_id."</data>
            <data key=\"v_name\">".str_replace('&', 'y', $nodo->v_name)."</data>
        </node>".PHP_EOL);
    }
    
    /* E S C R I T U R A  D E  A R I S T A S */
    
    foreach ($aristasSinRepetir as $arista) { 
        fwrite($file, "
        <edge source = \"".$arista->nodoOrigen."\" target=\"".$arista->nodoDestino."\">
        </edge>".PHP_EOL);
    }
           
    
     fwrite($file, "
       </graph>
</graphml>
     ".PHP_EOL); 
	fclose($file);


	
?>
