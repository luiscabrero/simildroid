<?php   
    //  -   -   -   -   -   -     -   -   -     -   -   -   -   -   -   -   -       -   -   -   -   -   -   -   -  //
    //  F   A   L   T   A   N     L   O   S     P   E   R   M   I   S   O   S       ¿   ¿   ¿   N   O   ?   ?   ?  //
    //  -   -   -   -   -   -     -   -   -     -   -   -   -   -   -   -   -       -   -   -   -   -   -   -   -  //
    //ini_set('memory_limit', '-1'); //http://stackoverflow.com/questions/561066/fatal-error-allowed-memory-size-of-134217728-bytes-exhausted-codeigniter-xml
    

    function traducirTags($tags) {//https://statickidz.com/proyectos/traductor-google-api-php-gratis/ 25-08-2016 at 19:22
	
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	require_once('GoogleTranslate.class.php');

	$source = 'es';
	$target = 'en';
	$text = '';

	for($i = 0; $i < count($tags); $i++){
		$text = $text.$tags[$i];
		if($i < count($tags)-1)
			$text = $text." / ";		
	}



	$translation = GoogleTranslate::translate($source, $target, $text); //traducimos

	$res = explode("/", $translation);
	return $res;
    }   

    /**
    * The similarity returned by this algorithm is a number between 0,1 (https://github.com/angeloskath/php-nlp-tools/blob/master/src/NlpTools 11/08/2016 at 14:22)
    */
/*
    function bin2base64($bin) {//http://stackoverflow.com/questions/7607611/convert-a-binary-number-to-base-64
        $arr = str_split($bin, 8);
        $str = '';
        foreach ( $arr as $binNumber ) {
            $str .= chr(bindec($binNumber));
        }
        return base64_encode($str);
    }
    function base64bin($str) {//http://stackoverflow.com/questions/7607611/convert-a-binary-number-to-base-64
        $result = '';
        $str = base64_decode($str);
        $len = strlen($str);
        for ( $n = 0; $n < $len; $n++ ) {
            $result .= str_pad(decbin(ord($str[$n])), 8, '0', STR_PAD_LEFT);
        }
        return $result;
    }
    */
    function cuantosUnosHay($string){

        $unos = 0;
        for($i = 0; $i < strlen($string); $i++)
            if($string[$i] === "1")
                $unos++;
        return $unos;
    }
    /**
     * Hemos elegido Levenstein, que devuelve el número de caracteres que habría que cambiar para 
     */
    function calcularLevenshteinNormalizado ($s1 ,$s2){
        $lev = levenshtein ($s1,$s2);

        return 1-$lev/(max(strlen($s1), strlen($s2)));
    }
    /*function permisos_String2B64($permisos){
        //asignamos valores, de manera que cada permiso asigna un bit distinto.
        $resArray = array();
        foreach($permisos as $p){

            switch ($p["Permiso_nombre"]) {
                case 'ACCESS_CHECKIN_PROPERTIES':           $resArray[144-138] = 1;break;
                case 'ACCESS_COARSE_LOCATION':              $resArray[144-137] = 1;break;
                case 'ACCESS_FINE_LOCATION':                $resArray[144-136] = 1;break;
                case 'ACCESS_LOCATION_EXTRA_COMMANDS':      $resArray[144-135] = 1;break;
                case 'ACCESS_NETWORK_STATE':                $resArray[144-134] = 1;break;
                case 'ACCESS_NOTIFICATION_POLICY':          $resArray[144-133] = 1;break;
                case 'ACCESS_WIFI_STATE':                   $resArray[144-132] = 1;break;
                case 'ACCESS_NOTIFICATION_POLICY':          $resArray[144-131] = 1;break;
                case 'ACCOUNT_MANAGER':                     $resArray[144-130] = 1;break;
                case 'ADD_VOICEMAIL':                       $resArray[144-129] = 1;break;
                case 'BATTERY_STATS':                       $resArray[144-128] = 1;break;
                case 'BIND_ACCESSIBILITY_SERVICE':          $resArray[144-127] = 1;break;
                case 'BIND_APPWIDGET':                      $resArray[144-126] = 1;break;
                case 'BIND_CARRIER_MESSAGING_SERVICE':      $resArray[144-125] = 1;break;
                case 'BIND_CARRIER_SERVICES':               $resArray[144-124] = 1;break;
                case 'BIND_CHOOSER_TARGET_SERVICE':         $resArray[144-123] = 1;break;
                case 'BIND_CONDITION_PROVIDER_SERVICE':     $resArray[144-122] = 1;break;
                case 'BIND_DEVICE_ADMIN':                   $resArray[144-121] = 1;break;
                case 'BIND_DREAM_SERVICE':                  $resArray[144-120] = 1;break;
                case 'BIND_INCALL_SERVICE':                 $resArray[144-119] = 1;break;
                case 'BIND_INPUT_METHOD':                   $resArray[144-118] = 1;break;
                case 'BIND_MIDI_DEVICE_SERVICE':            $resArray[144-117] = 1;break;
                case 'BIND_NFC_SERVICE':                    $resArray[144-116] = 1;break;
                case 'BIND_NOTIFICATION_LISTENER_SERVICE':  $resArray[144-115] = 1;break;
                case 'BIND_PRINT_SERVICE':                  $resArray[144-114] = 1;break;
                case 'BIND_QUICK_SETTINGS_TILE':            $resArray[144-113] = 1;break;
                case 'BIND_REMOTEVIEWS':                    $resArray[144-112] = 1;break;
                case 'BIND_SCREENING_SERVICE':              $resArray[144-111] = 1;break;
                case 'BIND_TELECOM_CONNECTION_SERVICE':     $resArray[144-110] = 1;break;
                case 'BIND_TEXT_SERVICE':                   $resArray[144-109] = 1;break;
                case 'BIND_TV_INPUT':                       $resArray[144-108] = 1;break;
                case 'BIND_VOICE_INTERACTION':              $resArray[144-107] = 1;break;
                case 'BIND_VPN_SERVICE':                    $resArray[144-106] = 1;break;
                case 'BIND_VR_LISTENER_SERVICE':            $resArray[144-105] = 1;break;
                case 'BIND_WALLPAPER':                      $resArray[144-104] = 1;break;
                case 'BLUETOOTH':                           $resArray[144-103] = 1;break;
                case 'BLUETOOTH_ADMIN':                     $resArray[144-102] = 1;break;
                case 'BLUETOOTH_PRIVILEGED':                $resArray[144-101] = 1;break;
                case 'BODY_SENSORS':                        $resArray[144-100] = 1;break;
                case 'BROADCAST_PACKAGE_REMOVED':           $resArray[144-99] = 1;break;
                case 'BROADCAST_SMS':                       $resArray[144-98] = 1;break;
                case 'BROADCAST_STICKY':                    $resArray[144-97] = 1;break;
                case 'BROADCAST_WAP_PUSH':                  $resArray[144-96] = 1;break;
                case 'CALL_PHONE':                          $resArray[144-95] = 1;break;
                case 'CALL_PRIVILEGED':                     $resArray[144-94] = 1;break;
                case 'CAMERA':                              $resArray[144-93] = 1;break;
                case 'CAPTURE_AUDIO_OUTPUT':                $resArray[144-92] = 1;break;
                case 'CAPTURE_SECURE_VIDEO_OUTPUT':         $resArray[144-91] = 1;break;
                case 'CAPTURE_VIDEO_OUTPUT':                $resArray[144-90] = 1;break;
                case 'CHANGE_COMPONENT_ENABLED_STATE':      $resArray[144-89] = 1;break;
                case 'CHANGE_CONFIGURATION':                $resArray[144-88] = 1;break;
                case 'CHANGE_NETWORK_STATE':                $resArray[144-87] = 1;break;
                case 'CHANGE_WIFI_MULTICAST_STATE':         $resArray[144-86] = 1;break;
                case 'CHANGE_WIFI_STATE':                   $resArray[144-85] = 1;break;
                case 'CLEAR_APP_CACHE':                     $resArray[144-84] = 1;break;
                case 'CONTROL_LOCATION_UPDATES':            $resArray[144-83] = 1;break;
                case 'DELETE_CACHE_FILES':                  $resArray[144-82] = 1;break;
                case 'DELETE_PACKAGES':                     $resArray[144-81] = 1;break;
                case 'DIAGNOSTIC':                          $resArray[144-80] = 1;break;
                case 'DISABLE_KEYGUARD':                    $resArray[144-79] = 1;break;
                case 'DUMP':                                $resArray[144-78] = 1;break;
                case 'EXPAND_STATUS_BAR':                   $resArray[144-77] = 1;break;
                case 'FACTORY_TEST':                        $resArray[144-76] = 1;break;
                case 'GET_ACCOUNTS':                        $resArray[144-75] = 1;break;
                case 'GET_ACCOUNTS_PRIVILEGED':             $resArray[144-74] = 1;break;
                case 'GET_PACKAGE_SIZE':                    $resArray[144-73] = 1;break;
                case 'GET_TASKS':                           $resArray[144-72] = 1;break;
                case 'GLOBAL_SEARCH';                       $resArray[144-71] = 1;break;
                case 'INSTALL_LOCATION_PROVIDER':           $resArray[144-70] = 1;break;
                case 'INSTALL_PACKAGES':                    $resArray[144-69] = 1;break;
                case 'INSTALL_SHORTCUT':                    $resArray[144-68] = 1;break;
                case 'INTERNET':                            $resArray[144-67] = 1;break;
                case 'KILL_BACKGROUND_PROCESSES':           $resArray[144-66] = 1;break;
                case 'LOCATION_HARDWARE':                   $resArray[144-65] = 1;break;
                case 'MANAGE_DOCUMENTS':                    $resArray[144-64] = 1;break;
                case 'MASTER_CLEAR':                        $resArray[144-63] = 1;break;
                case 'MEDIA_CONTENT_CONTROL':               $resArray[144-62] = 1;break;
                case 'MODIFY_AUDIO_SETTINGS':               $resArray[144-61] = 1;break;
                case 'MODIFY_PHONE_STATE':                  $resArray[144-60] = 1;break;
                case 'MOUNT_FORMAT_FILESYSTEMS':            $resArray[144-59] = 1;break;
                case 'MOUNT_UNMOUNT_FILESYSTEMS':           $resArray[144-58] = 1;break;
                case 'NFC':                                 $resArray[144-57] = 1;break;
                case 'PACKAGE_USAGE_STATS':                 $resArray[144-56] = 1;break;
                case 'PERSISTENT_ACTIVITY':                 $resArray[144-55] = 1;break;
                case 'PROCESS_OUTGOING_CALLS':              $resArray[144-54] = 1;break;
                case 'READ_CALENDAR':                       $resArray[144-53] = 1;break;
                case 'READ_CALL_LOG':                       $resArray[144-52] = 1;break;
                case 'READ_CONTACTS':                       $resArray[144-51] = 1;break;
                case 'READ_EXTERNAL_STORAGE':               $resArray[144-50] = 1;break;
                case 'READ_FRAME_BUFFER':                   $resArray[144-49] = 1;break;
                case 'READ_INPUT_STATE':                    $resArray[144-48] = 1;break;
                case 'READ_LOGS':                           $resArray[144-47] = 1;break;
                case 'READ_PHONE_STATE':                    $resArray[144-46] = 1;break;
                case 'READ_SMS':                            $resArray[144-45] = 1;break;
                case 'READ_SYNC_SETTINGS':                  $resArray[144-44] = 1;break;
                case 'READ_SYNC_STATS':                     $resArray[144-43] = 1;break;
                case 'READ_VOICEMAIL':                      $resArray[144-42] = 1;break;
                case 'REBOOT':                              $resArray[144-41] = 1;break;
                case 'RECEIVE_BOOT_COMPLETED':              $resArray[144-40] = 1;break;
                case 'RECEIVE_MMS':                         $resArray[144-39] = 1;break;
                case 'RECEIVE_SMS':                         $resArray[144-38] = 1;break;
                case 'RECEIVE_WAP_PUSH':                    $resArray[144-37] = 1;break;
                case 'RECORD_AUDIO':                        $resArray[144-36] = 1;break;
                case 'REORDER_TASKS':                       $resArray[144-35] = 1;break;
                case 'REQUEST_IGNORE_BATTERY_OPTIMIZATIONS':$resArray[144-34] = 1;break;
                case 'REQUEST_INSTALL_PACKAGES':            $resArray[144-33] = 1;break;
                case 'RESTART_PACKAGES':                    $resArray[144-32] = 1;break;
                case 'SEND_RESPOND_VIA_MESSAGE':            $resArray[144-31] = 1;break;
                case 'SEND_SMS':                            $resArray[144-30] = 1;break;
                case 'SET_ALARM':                           $resArray[144-29] = 1;break;
                case 'SET_ALWAYS_FINISH':                   $resArray[144-28] = 1;break;
                case 'SET_ANIMATION_SCALE':                 $resArray[144-27] = 1;break;
                case 'SET_DEBUG_APP':                       $resArray[144-26] = 1;break;
                case 'SET_PREFERRED_APPLICATIONS':          $resArray[144-25] = 1;break;
                case 'SET_PROCESS_LIMIT':                   $resArray[144-24] = 1;break;
                case 'SET_TIME':                            $resArray[144-23] = 1;break;
                case 'SET_TIME_ZONE':                       $resArray[144-22] = 1;break;
                case 'SET_WALLPAPER':                       $resArray[144-21] = 1;break;
                case 'SET_WALLPAPER_HINTS':                 $resArray[144-20] = 1;break;
                case 'SIGNAL_PERSISTENT_PROCESSES':         $resArray[144-19] = 1;break;
                case 'STATUS_BAR':                          $resArray[144-18] = 1;break;
                case 'SYSTEM_ALERT_WINDOW':                 $resArray[144-17] = 1;break;
                case 'TRANSMIT_IR':                         $resArray[144-16] = 1;break;
                case 'UNINSTALL_SHORTCUT':                  $resArray[144-15] = 1;break;
                case 'UPDATE_DEVICE_STATS':                 $resArray[144-14] = 1;break;
                case 'USE_FINGERPRINT':                     $resArray[144-13] = 1;break;
                case 'USE_SIP':                             $resArray[144-12] = 1;break;
                case 'VIBRATE':                             $resArray[144-11] = 1;break;
                case 'WAKE_LOCK':                           $resArray[144-10] = 1;break;
                case 'WRITE_APN_SETTINGS':                  $resArray[144-9] = 1;break;
                case 'WRITE_CALENDAR':                      $resArray[144-8] = 1;break;
                case 'WRITE_CALL_LOG':                      $resArray[144-7] = 1;break;
                case 'WRITE_CONTACTS':                      $resArray[144-6] = 1;break;
                case 'WRITE_EXTERNAL_STORAGE':              $resArray[144-5] = 1;break;
                case 'WRITE_GSERVICES':                     $resArray[144-4] = 1;break;
                case 'WRITE_SECURE_SETTINGS':               $resArray[144-3] = 1;break;
                case 'WRITE_SYNC_SETTINGS':                 $resArray[144-2] = 1;break;
                case 'WRITE_SETTINGS':                      $resArray[144-1] = 1;break;
            }
        }
        
        //unificamos para encontrar el array de bit completo
        for($i = 0; $i < 144; $i++){
            if($resArray[$i] === 1)
                $res = $res."1";
            else {
                $res = $res."0";
            }
        }
        return bin2base64($res);
    }*/
    
    function calcularInterseccion($a, $b)
    {
       $iguales = 0;
       if(strlen($a) != strlen($b))
          return -1;
       else{
           for($i = 0; $i < strlen($a); $i++){
               if($a[$i] === $b[$i] && $a[$i] === "1")
                $iguales++;
           }
       }
        return $iguales;
    }
    
    
    
	session_start();
	if($_SESSION["nick"] == null)
		header("Location: ../index.php?error=nCn");
		
    //include './BBDD.php'; //incluimos este fichero para poder hacer llamadas a la base de datos.
    //include './graphmlObjects.php'; //incluimos este fichero para poder crear objetos de tipo Nodo y Arista.
    
    //$hoy = getdate(); //obtenemos fecha para crear el fichero personalizado
    $file = fopen("./data/grafo_similitudes.graphml", "w"); //abrimos el fichero donde vamos a generar el contenido del GraphML

    //generamos la parte común del fichero, todos los GML tendrán estas primeras líneas comunes.
    fwrite($file, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
    <graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\"
    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
    xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns
    http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\">

    <key id=\"v_name\" for=\"node\" attr.name=\"name\" attr.type=\"string\"/>
    <key id=\"v_nt\" for=\"node\" attr.name=\"nt\" attr.type=\"string\"/>
    <key id=\"v_myid\" for=\"node\" attr.name=\"myid\" attr.type=\"string\"/>
    <key id=\"v_ft\" for=\"node\" attr.name=\"ft\" attr.type=\"string\"/>
    <key id=\"v_tt\" for=\"node\" attr.name=\"tt\" attr.type=\"string\"/>
    <key id=\"v_st\" for=\"node\" attr.name=\"st\" attr.type=\"string\"/>
    <key id=\"v_id\" for=\"node\" attr.name=\"id\" attr.type=\"string\"/>
    <key id=\"e_sw\" for=\"edge\" attr.name=\"sw\" attr.type=\"double\"/>
    <graph id=\"G\" edgedefault=\"undirected\">".PHP_EOL); 
        
    /*
    * Inicializamos estructuras de datos necesarias para el correcto funcionamiento del código
    */
    $nodos = array();
    $aristas = array();
    $categorias = array();
    $permisos = array();
    $appsNombres = array();
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /*  S U B G R A F O   C O R R E S P O N D I E N T E   A   L A S   C A T E G O R I A S   */
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    
    //Como este subgrafo será precalculado, lo haremos "a mano"
     $file2 = fopen("./data/categorias_similitudes.csv", "r");
	 while(!feof($file2)){
	     
            $linea = fgets($file2); // la linea completa
            $cat1 = substr($linea, 0, stripos($linea, ";")); //categoria 1
            $sublinea = substr($linea, stripos($linea, ";")+1); // linea - cat1
            $cat2 = substr($sublinea,0, stripos($sublinea, ";")); //categoria 2
            $simil = substr($linea,strripos($linea, ";")+1); //similitud entre categoria1 y categoria2
            
            $existeCategoria = array_search($cat1, $categorias);
            if($existeCategoria === FALSE){ //en caso de no existir, creamos el nodo correspondiente a categoria1
                $nuevoNodo = new Nodo();
                $nuevoNodo->v_nt = "fea"; //nodo de tipo Terminal
                $nuevoNodo->v_myid = $cat1;
                $nuevoNodo->v_id = "n".sizeof($nodos);
                $nuevoNodo->v_name = $cat1;
                array_push($nodos,$nuevoNodo); //insertamos el nodo en el conjunto de nodos
                array_push($categorias,$cat1);
                $nodoA = $nuevoNodo->v_id;
            }else{//si existe, hay que buscarlo :S
                foreach($nodos as $n)
                    if($n->v_name === $cat1)
                        $nodoA = $n->v_id;
            }
            
            //realizamos la misma operacion para categoria2            
            $existeCategoria = array_search($cat2, $categorias);
            if($existeCategoria === FALSE){ //en caso de no existir, creamos el nodo correspondiente a categoria1
                $nuevoNodo = new Nodo();
                $nuevoNodo->v_nt = "fea"; //nodo de tipo Terminal
                $nuevoNodo->v_myid = $cat2;
                $nuevoNodo->v_id = "n".sizeof($nodos);
                $nuevoNodo->v_name = $cat2;
                array_push($nodos,$nuevoNodo); //insertamos el nodo en el conjunto de nodos
                array_push($categorias,$cat2);
                $nodoB = $nuevoNodo->v_id;
            }else{//si existe, hay que buscarlo :S
                foreach($nodos as $n)
                    if($n->v_name === $cat2)
                        $nodoB = $n->v_id;
            }
            
            //ahora que tenemos o creado el nodo de las dos categorias, o encontrados los id's, podemos crear la conexión entre ellos (arista)
            $nuevaArista = new Arista();
            $nuevaArista->nodoOrigen = $nodoA; //nodo terminal 
            $nuevaArista->nodoDestino = $nodoB; //nodo app
            $nuevaArista->peso = $simil;
            array_push($aristas,$nuevaArista);

    }
    fclose($file2);

 
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /*  S U B G R A F O   C O R R E S P O N D I E N T E   A   L O S  P E R M I S O S        */
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    
    //use \NlpTools\Similarity\JaccardIndex;  //https://github.com/angeloskath/php-nlp-tools/
    
    //Primer paso, obtener las aplicaciones del usuario.
    $BBDD = new BBDD();
    $aplicaciones = $BBDD->obtenerTodasAplicacionesPorUsuario($_SESSION["nick"]); //obtenemos apps de este terminal
    $BBDD->terminarConexion();
    //Segundo paso, obtener los mismos permisos que obteniamos en generarGraphML.php, para así, poder calcular la similitud :D
    
    foreach($aplicaciones as $aplicacion){
        $BBDD = new BBDD();
        $permisosApp = $BBDD->obtenerPermisos($aplicacion["idTerminal"],$aplicacion["nombre"]);//obtenemos etiquetas de esta app
        $BBDD->terminarConexion();
        $permisos_B64_app = permisos_String2B64($permisosApp); //obtenemos permisos para la app en b64.
        if(array_search($permisos_B64_app, $permisos) === FALSE) // creamos una lista que contenga una sola vez todos los permisos. (en caso de haber permisos iguales, no los intruce dos veces)
            array_push($permisos, $permisos_B64_app);
    }   
    
    //aquí ya tendremos todos los permisos posibles del usuario. Por lo que ahora, tendremos que: por cada grupo de permisos, 
                
    foreach($permisos as $permiso){//para cada permiso, generamos un nodo (nos hemos asegurado ya de que no hay dos permisos iguales.)
        $nuevoNodo = new Nodo(); //generamos el nodo
        $nuevoNodo->v_nt = "fea"; 
        $nuevoNodo->v_myid = $permiso;
        $nuevoNodo->v_id = "n".sizeof($nodos);
        $nuevoNodo->v_name = $permiso;
        $nuevoNodo->tipo_feature ="permiso";
        array_push($nodos,$nuevoNodo);    
    }
    
    //ahora, para cada permiso, tendremos que crear una arista que conecte dicho permiso, con todos los demás (llamando a la funcion Jaccard para obtener la similitud entre permisos)
    
    for($i = 0; $i < sizeof($permisos); $i++){
        for($j = $i; $j < sizeof($permisos); $j++){ //al hacer $j = $i, evitamos repetir combinaciones de permisos.
            if($i != $j){//para todos aquellos permisos que no sean el mismo permiso (no tiene sentido obtener similitud de un permiso con el mismo)
                
                $modA = cuantosUnosHay(base64bin($permisos[$i])); //para la formula de Jaccard, esto es |A|
                $modB = cuantosUnosHay(base64bin($permisos[$j])); //para la formula de Jaccard, esto es |B|
                
                $inter = calcularInterseccion(base64bin($permisos[$i]), base64bin($permisos[$j]));
                
                $simil = ($inter)/($modA+$modB-$inter);
                $nodoA = $nodoB = "-1";
                
                foreach($nodos as $n){
                    if($n->tipo_feature === "permiso" && $n->v_name === $permisos[$i]){
                        $nodoA = $n->v_id;
                    }
                    if($n->tipo_feature === "permiso" && $n->v_name === $permisos[$j]){
                        $nodoB = $n->v_id;
                    }
                }

                //ahora que tenemos o creado el nodo de los dos permisos, o encontrados los id's, podemos crear la conexión entre ellos (arista)
                $nuevaArista = new Arista();
                $nuevaArista->nodoOrigen = $nodoA;
                $nuevaArista->nodoDestino = $nodoB;
                $nuevaArista->peso = $simil;
                array_push($aristas,$nuevaArista);
            
            }
        }
    }
   
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /*  S U B G R A F O   C O R R E S P O N D I E N T E   A   L O S  N O M B R E S          */ //nombres de las apps $appsNombres
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    foreach($aplicaciones as $aplicacion){

        if(array_search($aplicacion["nombre"], $appsNombres) === FALSE) // creamos una lista que contenga una sola vez todos los nombres. (en caso de haber nombres iguales, no se intruce dos veces)
            array_push($appsNombres, $aplicacion["nombre"]);
    }   
    
    //aquí ya tendremos todos los nombres posibles de las apps. Por lo que ahora, tendremos que: por cada nombre, 
    foreach($appsNombres as $nombre){//generamos un nodo (nos hemos asegurado ya de que no hay dos nombres iguales.)
        $nuevoNodo = new Nodo(); //generamos el nodo
        $nuevoNodo->v_nt = "fea"; 
        $nuevoNodo->v_myid = $nombre;
        $nuevoNodo->v_id = "n".sizeof($nodos);
        $nuevoNodo->v_name = $nombre;
        $nuevoNodo->tipo_feature ="nombre";
        array_push($nodos,$nuevoNodo);    
    }
    
    //ahora, para cada nombre, tendremos que crear una arista que conecte dicho nombre, con todos los demás
    
    for($i = 0; $i < sizeof($appsNombres); $i++){
        for($j = $i; $j < sizeof($appsNombres); $j++){ //al hacer $j = $i, evitamos repetir combinaciones de nombres.
            if($i != $j){//para todos aquellos permisos que no sean el mismo nombre (no tiene sentido obtener similitud de un nombre con el mismo)
                
                $simil = calcularLevenshteinNormalizado($appsNombres[$i], $appsNombres[$j]);
               
                foreach($nodos as $n){
                    if($n->tipo_feature === "nombre" && $n->v_name === $appsNombres[$i]){
                        $nodoA = $n->v_id;
                    }
                    if($n->tipo_feature === "nombre" && $n->v_name === $appsNombres[$j]){
                        $nodoB = $n->v_id;
                    }
                }
                
                //ahora que tenemos o creado el nodo de los dos nombres, o encontrados los id's, podemos crear la conexión entre ellos (arista)
                $nuevaArista = new Arista();
                $nuevaArista->nodoOrigen = $nodoA;
                $nuevaArista->nodoDestino = $nodoB;
                $nuevaArista->peso = $simil;
                array_push($aristas,$nuevaArista);
            }
        }
    }

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    /*  S U B G R A F O   C O R R E S P O N D I E N T E   A   L O S  T A G S                */ //nombres de los tags
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/
    

    //Primer paso, obtener los tags del usuario.
    $BBDD = new BBDD();
    $tags = $BBDD->obtenerEtiquetasPorUsuario($_SESSION["nick"]); //obtenemos tags de este usuario
    $BBDD->terminarConexion();
    $tagsArray = array();
   
    $numeroDeTags = 0; 
    foreach($tags as $tag){//generar un nodo 
        array_push($tagsArray,$tag["Etiqueta_nombre"]);  
	$numeroDeTags++;
    }
    
    $tagsArrayTraducidos = traducirTags($tagsArray); 
    $infoTags = array(
		"nombre" => array(),
		"nombreEN" => array(),
		"idNodo" => array()
    );

    $k = 0;
    foreach($tags as $tag){//generar un nodo 
        $nuevoNodo = new Nodo(); //generamos el nodo
        $nuevoNodo->v_nt = "tag"; 
        $nuevoNodo->v_myid = "t:".$tag["Etiqueta_nombre"];
        $nuevoNodo->v_id = "n".sizeof($nodos);
        $nuevoNodo->v_name = "t:".$tag["Etiqueta_nombre"];
        $nuevoNodo->v_nameEN = trim($tagsArrayTraducidos[$k]," ");
        $nuevoNodo->tipo_feature ="tag";

	array_push($infoTags["nombre"],"t:".$tag["Etiqueta_nombre"]);
	array_push($infoTags["nombreEN"],strtolower(trim($tagsArrayTraducidos[$k]," ")));
	array_push($infoTags["idNodo"],"n".sizeof($nodos));
        array_push($nodos,$nuevoNodo);
	$k++;
    }


    //generamos el fichero .csv que necesitará semilar para calcular la similitud entre tags.
    $fileSIMIL = fopen("../semilar/Semilar-1.0.2/Word2Word-Similarity-test-data/semantic-pairs-prueba-corto.csv", "w"); //abrimos el fichero donde vamos a generar el contenido del GraphML 
    



    for($a = 0; $a < count($tagsArrayTraducidos); $a++){ //generamos el fichero para poder mandar la peticion a SEMILAR sobre las similitudes de los tags
        for($b = $a; $b < count($tagsArrayTraducidos); $b++){ //al hacer $b = $a, evitamos repetir combinaciones de tags.
            if($a != $b){//para todos aquellos tags que no sean el mismo tag (no tiene sentido obtener similitud de un tag con el mismo)
		$linea = trim($tagsArrayTraducidos[$a]," ").";".trim($tagsArrayTraducidos[$b]," ");

		fwrite($fileSIMIL, $linea.PHP_EOL);
            }
        }
    }
    fclose($fileSIMIL);

    $ejecutar = "./ejecutarJAVA.sh";

    $ultima_linea = exec($ejecutar);


    //ahora leemos el fichero generado .csv que contiene las similitudes calculadas por semilar.
    $fileSIMILres = fopen("../semilar/Semilar-1.0.2/Word2Word-Similarity-test-data/prueba-output.csv", "r"); //abrimos el fichero donde vamos a generar el contenido del GraphML 
    
    $linea = fgets($fileSIMILres); // la primera linea no sirve, ya que solo contiene el esquema del resultado 
    while(!feof($fileSIMILres)){
	    $linea = fgets($fileSIMILres);
	    if($linea != ""){
	    		
		$simil = substr(strrchr($linea, ";"), 1); // obtenemos la similitud
		$tagA = substr($linea,0,strpos($linea,";")); // obtenemos el tag A
		$tagB = substr($linea,strpos($linea,";")+1,strrpos($linea,";") - strpos($linea,";") - 1); //obtenemos el tag B
		
		print ("VAMOS A CREAR LA ARISTA DESDE: ".$tagA." hasta ".$tagB."<br>");
		$a = array_search($tagA,$infoTags["nombreEN"]);
		$b = array_search($tagB,$infoTags["nombreEN"]);


                //ahora que tenemos o creado el nodo de los dos nombres, o encontrados los id's, podemos crear la conexión entre ellos (arista)
                $nuevaArista = new Arista();
                $nuevaArista->nodoOrigen = $infoTags["idNodo"][$a];
                $nuevaArista->nodoDestino = $infoTags["idNodo"][$b];
                $nuevaArista->peso = $simil;
                array_push($aristas,$nuevaArista);
	    }
    }
	
    fclose($fileSIMILres);
    unlink("../semilar/Semilar-1.0.2/Word2Word-Similarity-test-data/prueba-output.csv");  //HAY QUE ELIMINARLO PARA EVITAR PROBLEMAS
    
    
    //tras haber creado todos los nodos y las aristas, ahora debemos escribirlos en el .graphml
    print (" --------------->> GRAFO ESTRUCTURAL <<--------------- ");
    print(" # # # # # # # # # # # # # #  N  O  D  O  S  # # # # # # # # # # # # # # <br>");

    foreach($nodos as $nodo){
        print("Existe un nodo con id: ".$nodo->v_id." y nombre: ".str_replace('&', 'y', $nodo->v_name)." <br>");
    }
    
    print(" # # # # # # # # # # # # # #  A   R   I   S   T   A   S  2.0# # # # # # # # # # # # # # ".sizeof($aristasSinRepetir)."<br>");

    foreach($aristas as $arista){
        print("Existe una arista que va desde ".$arista->nodoOrigen." hasta ".$arista->nodoDestino." y tiene id= ".$arista->id."<br>");
    }
    /* E S C R I T U R A  D E  N O D O S */
    
    foreach ($nodos as $nodo) { 
        fwrite($file, "
        <node id=\"".$nodo->v_id."\">
            <data key=\"v_nt\">".$nodo->v_nt."</data>
            <data key=\"v_myid\">".str_replace('&', 'y', $nodo->v_myid)."</data>
            <data key=\"v_ft\">".$nodo->v_ft."</data>
            <data key=\"v_tt\">".$nodo->v_tt."</data>
            <data key=\"v_st\">".$nodo->v_st."</data>
            <data key=\"v_id\">".$nodo->v_id."</data>
            <data key=\"v_name\">".str_replace('&', 'y', $nodo->v_name)."</data>
        </node>".PHP_EOL);
    }
    
    /* E S C R I T U R A  D E  A R I S T A S */
    
    foreach ($aristas as $arista) { 
        fwrite($file, "
        <edge source = \"".$arista->nodoOrigen."\" target=\"".$arista->nodoDestino."\">
            <data key=\"e_sw\">".$arista->peso."</data>
        </edge>".PHP_EOL);
    }
    
     fwrite($file, "
       </graph>
</graphml>
     ".PHP_EOL); 
	fclose($file);
	?>
