<?php 
    class Nodo {
        public $v_nt;
        public $v_myid;
        public $v_ft;
        public $v_tt;
        public $v_st;
        public $v_id;
        public $v_name;
        public $tipo_feature;
	public $v_nameEN;
    }
    class Arista {
	public $id; //no hacer caso al valor que toma este id en la arista, solo sirve para eliminar las aristas duplicadas, para nada más.
        public $nodoOrigen;
        public $nodoDestino;
        public $peso;
    }
?>
