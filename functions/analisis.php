<?php
	error_reporting(0);
	
	include './BBDD.php';
	
	session_start();
	if($_SESSION["nick"] == null)
		header("Location: ../index.php?error=nCn");
	
	function consultarCategorias(){
		$BBDD = new BBDD();
		//aqui tenemos todas las aplicaciones del sistema que no están categorizadas.
		$aplicacionesSinCategorizar = $BBDD->obtenerPaquetesSinCategoria();
		$options = array(
			'http' => array(
			'method'  => 'GET',
			//'content' => json_encode( $data ),
			'header'=>  "Content-Type: application/json\r\n" .
				"Accept: application/json\r\n"
			)
		);
		//creamos un array que contenga el nombre de todos los paquetes (necesario para enviar información)
		$i = 0;
		foreach ($aplicacionesSinCategorizar as $app){
			$i++;
			$url = 'https://42matters.com/api/1/apps/lookup.json?p='.$app[Aplicacion_nombre].'&access_token=6c3cbd96e6ae40f55f787f1ac244fdee8ce9c3d8&lang=ES';
			$context  = stream_context_create( $options );
			$result = file_get_contents( $url, false, $context );
			$response = json_decode( $result );
			if($response){ //solo de las que se pueda!!!!!!!!
				$BBDD->guardarNuevasCategorias($app[Aplicacion_nombre], $response->category);
				$BBDD->asignarAutor($app[Aplicacion_nombre], $response->developer);
			}else{
				$BBDD->guardarNuevasCategorias($app[Aplicacion_nombre], "Unknown");
				$BBDD->asignarAutor($app[Aplicacion_nombre], "Unknown");
			}
        	}
        	$BBDD->terminarConexion();
        
    	}
    	consultarCategorias();

	//header('Location: generarGraphML.php');
	
	//ahora añadimos la peticion de tarea a la bbdd.
	$BBDD = new BBDD();
	$res = $BBDD->almacenarTarea($_SESSION["nick"]);	
	//include './generaGraphML_similitudes.php';
?>
