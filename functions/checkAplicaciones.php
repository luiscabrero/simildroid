<?php
error_reporting(0);
$dbhost = "localhost";
$database="mydb";
$user="root";
$password="admin";

// Creacion de conexion
$conn = new mysqli($dbhost, $user, $password, $database);

// Comprobacion de conexion 
if ($conn->connect_error) {
    die("Error en la conexion a MySQL: " . $conn->connect_error);
}

//definimos los parámetros del apartado anterior --> CAMBIARLO POR JSON

$json = file_get_contents('php://input'); // Recoge el JSON QUE LE LLEGA
$checkApps = json_decode($json); // aqui tenemos ya el json decodificado

//Paso 1: Obtener valores para buscar el terminal.
$nick = $checkApps->usr;
$pwd = $checkApps->pwd;
$terminal = $checkApps->idTerminal; //realmente es el nombre

$idTerminal = 1;
$idSiguienteTerminal = 1;
	//Paso 2: Buscar el terminal, si no existe crearlo.
	if($stmt = $conn->prepare("SELECT * FROM Terminal WHERE Usuario_nick = ? AND nombre = ?")){
	    $stmt->bind_param("ss", $nick, $terminal);
	    
	    //ejecutamos la consulta
	    $stmt->execute();
	    
	    $stmt->store_result();
	    //vincular las variables de resultado
	    $stmt->bind_result($idTerminal, $nombreTerminal, $tipo, $nombreUsuario);
	    //obtenemos el valor
	    $stmt->fetch();

	    if($stmt->num_rows == 0){
		//Terminal nuevo, hay que crear un nuevo terminal. En primer lugar vemos cual es el ultimo ID introducido.
			if($stmt = $conn->prepare("SELECT MAX(idTerminal) AS id FROM Terminal")){
			    //ejecutamos la consulta
			    $stmt->execute();
			    $stmt->store_result();
			    //vincular las variables de resultado
			    $stmt->bind_result($idTerminal);
			    //obtenemos el valor
			    $stmt->fetch();
			}
			
			$idSiguienteTerminal = $idTerminal+1;
			if($stmt = $conn->prepare("INSERT INTO Terminal VALUES (?, ?, ?, 'movil', null)")){
			    if(!$stmt->bind_param("iss",$idSiguienteTerminal, $nick,$terminal))
			    	print("Error al crear la consulta para insertar terminal.");
			    //ejecutamos la consulta
			    $stmt->execute();
					
			}else{
			    //problemas con la sentencia
			    print("Error al insertar el terminal");
			}
			
	    }  
	}else{
	    print("Error al buscar el terminal");
	}

	$idTerminal = max($idSiguienteTerminal,$idTerminal);
	
	//Paso 3: Para el terminal Creado / Encontrado; Insertar todas y cada una de las aplicaciones encontradas.
	//Para ello tendremos que mirar todas las aplicaciones 
	$aplicaciones = $checkApps->aplicaciones;

	foreach($aplicaciones as $apps){
		$nApp = $apps->nombre;
		$paquete = $apps->paquete;
		if($stmt = $conn->prepare("INSERT INTO Aplicacion VALUES (?, ?, ?, null)")){
		    if(!$stmt->bind_param("ssi", $nApp,$paquete, $idTerminal)) 
		    	print("Error al crear la consulta para insertar aplicacion.");
		    
		    //ejecutamos la consulta
		    $stmt->execute();
				
		}else{
			print("Error al intertar aplicaciones");
		}
		//Paso 4: Ahora para cada app, almacenamos sus permisos.
		$permisos = $apps->permisos;
		foreach($permisos as $perms){
			//Paso 4.1: Primero debemos insertar los permisos en la tabla de permisos (EN CASO DE QUE NO EXISTAN o NO ESTÉN ALMACENADOS)
			/*if($stmt2 = $conn->prepare("INSERT INTO Permiso VALUES (?)")){
			    $stmt2->bind_param("s", $perms);
			    //ejecutamos la consulta
			    $stmt2->execute();
			    $idPermiso = $conn->insert_id;
			}*/ // se supone que van a estar PRE-CARGADOSSSSSSSSSSSSSS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			
			/*else{//debería de fallar cuando YA EXISTE EL PERMISO CON ESE NOMBRE!!!!!!!!!!
				//Paso 4.2: obtener el id del permiso para después poder introducir una entrada en la tabla Aplicacion_Permiso
				if($stmt3 = $conn->prepare("SELECT * FROM Permiso WHERE nombre = ?")){
				    $stmt3->bind_param("s", $perms);
				    
				    //ejecutamos la consulta
				    $stmt3->execute();
				    
				    $stmt3->store_result();
				    //vincular las variables de resultado
				    $stmt3->bind_result($idPermiso, $nombrePermiso);
				    //obtenemos el valor
				    $stmt3->fetch();
				}	
			}*/
			
			//Paso 4.3: En segundo lugar, debemos insertar la relación entre la aplicación y los permisos.
			if($stmt2 = $conn->prepare("INSERT INTO Aplicacion_Permiso VALUES (?,?,?)")){
			    $stmt2->bind_param("sss", $nApp, $idTerminal, substr($perms, strripos($perms, ".")+1));
			    //ejecutamos la consulta
			    $stmt2->execute();
			}
		}
		//Paso 4.4: Tenemos que ver si existe la aplicación en la tabla Aplicacion_Categoria, si no existe, se inserta.
		if($stmt2 = $conn->prepare("INSERT INTO Aplicacion_Categoria VALUES (null,?)")){
		    $stmt2->bind_param("s", $paquete);
		    //ejecutamos la consulta
		    $stmt2->execute();
		}
	}
	print "ok";
	return "ok";
// Cerrar la conexión
$conn->close();
?>
