<?php
$url = 'http://api.wheredatapp.com/data?key=5b983c0af023c29eb8b5483393c3ed3d3b77cd';
 
$data = array(
    "packages" => array("com.facebook","snooze.ninja")
);
   
 $options = array(
  'http' => array(
    'method'  => 'POST',
    'content' => json_encode( $data ),
    'header'=>  "Content-Type: application/json\r\n" .
                "Accept: application/json\r\n"
    )
);

$context  = stream_context_create( $options );
$result = file_get_contents( $url, false, $context );
$response = json_decode( $result );

foreach ($response->apps as $app){
    print("El paquete: ");
    print($app->package);
    print(" pertenece a la categoría: ");
    print($app->category);
    print("\n");
}    
?>
