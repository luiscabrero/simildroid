<?php

$dbhost = "localhost";
$database="mydb";
$user="root";
$password="admin";

// Creacion de conexion
$conn = new mysqli($dbhost, $user, $password, $database);

// Comprobacion de conexion 
if ($conn->connect_error) {
    die("Error en la conexion a MySQL: " . $conn->connect_error);
}

//definimos los parámetros del apartado anterior
$nick = $_POST["Nickname"];
$pass = hash("sha256",$_POST["Password"]);
//preparacion de la consulta
if($stmt = $conn->prepare("SELECT * FROM Usuario WHERE nick = ? and password= ?")){
    $stmt->bind_param("ss", $nick, $pass);
    //ejecutamos la consulta
    $stmt->execute();
    
    $stmt->store_result();
    //vincular las variables de resultado
    $stmt->bind_result($nickRes, $passwordRes, $emailRes);
    //obtenemos el valor
    $stmt->fetch();

    if($stmt->num_rows == 0){
      printf("El usuario no existe o la contraseña es incorrecta");
      printf("La pass que envias es: %s y la que tengo es: %s",hash("sha256",$_POST["Password"]),$passwordRes);
      header("Location: ../index.php?error=uNF");
    }else{    
      printf("El usuario no existe o la contraseña es incorrecta");
      printf("La pass que envias es: %s y la que tengo es: %s",hash("sha256",$_POST["Password"]),$passwordRes);
      session_start();
      $_SESSION["nick"] = $nickRes;
      header("Location: ../home.php");
    }
    
}else{
    printf("Error al buscar el usuario");
}
// Cerrar la conexión
$conn->close();
?>
