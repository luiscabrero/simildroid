{
##########################################################################################################
#    codigo que calcula la similitud/distancia entre los vertices de un grafo aumentado para la IoY
##########################################################################################################
# autor: anabel glez-tablas
# fecha: 13.07.2016 
##########################################################################################################
#                                           instrucciones
# 
# Se necesita como entrada un fichero graphml que contenga nodos y enlaces estructurales
# Los nodos deben contener informacion adicional en sus atributos (properties.data):
# 	"nt"= {"str", "tag", "fea"} --- NECESARIO 
#   "st"= {"app", "dev", "res"} --- NECESARIO "app"
#   "ft"= {"dev_type", "app_name", "app_category", "app_permissionset", "app_author", "res_type"} --- NECESARIO
#   "tt"= {"t:tag-user-defined-1", "t:tag-user-defined-2",...} --- NECESARIO
#   Estos tipos deben defininirse en el fichero "setDataTypes" correspondiente (de momento)
#   Se usan para calcular las probabilidades de transición. Son atributos críticos para el algoritmo.
# 	"myid" = identificador del nodo (semantico, eg: "a1" para una app, "private" para una tag...) -- NECESARIO
#
# Ademas se necesita como entrada otro fichero graphml que contenga los nodos tags y features y enlaces entre los
# del mismo tipo con un peso = su similitud. Este peso debe reflejarse en un atributo "sw" --- NECESARIO  

# OJO: PARA QUE NO HAYA PROBLEMAS AL UNIR LOS DOS GRAFICOS, COMO LO HACE POR EL ATRIBUTO NAME, PRIMERO LO ASIGNAMOS
# IGUAL AL ATRIBUTO myid. COMO HAY VALORES QUE COINCIDEN (TAGS LIBREMENTE ASIGNADAS) DIFERENCIAMOS EN YED LOS 
# VALORES DE LAS TAGS EN EL ATRIBUTO myid PRECEDIENDOLOS DE "t:     ". 
# Asi se unen sin problemas ambos grafos en R conservando los atributos.  
}

{
library(igraph)
library(hexbin)
library(xlsx)
library(calibrate)
library(cluster)
library(fpc)
}

#FLAGS PPUEBAS -- TRAZAS
enpruebas <- FALSE
pruebas2 <- FALSE
nacho <- FALSE
nacho2 <- FALSE
nacho3 <- FALSE
aniversario <- FALSE
DepuroAjustePesos <- FALSE
DepuroAjustePesos2 <- FALSE


CalculoSimilarityMatrix <- TRUE
PruebasIntegracion <- TRUE

source("utilsFiles.R")
source("utilsGraphs.R")
source("randomWalkSimilarity.R")
source("appClustering.R")
source("weightUpdate.R")

source("tagSpecification.R")
source("setDataTypes.R") 	# "importa" los tipos de nodos, features y tags:: vectores ntv, ftv y ttv
source("setInitialWeights.R") 	# importa" los pesos:: vector wv 

source("pathSpecification.R")

yed_gas <- FALSE 
yed_gs <- FALSE
graphPrefix <- "graphml/"
lengthWalk <- ceiling(diameter(gu)/2)
restartProbability <- 0.15
sigma <- 0.5 #user parameter for weight adjustment iteration
distPrefix <- "dist/"
clustPrefix <- "clust/"

ITERACIONES_MAX <- 30 #maximo de iteraciones


if (!PruebasIntegracion) {
cat("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
cat("*****************************************************************************************************************************************************\n")
cat("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
cat("*****************************************************************************************************************************************************\n")
cat("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
cat("*****************************************************************************************************************************************************\n")
cat("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
cat("*****************************************************************************************************************************************************\n")
cat("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
cat("*****************************************************************************************************************************************************\n")
cat("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
cat("*****************************************************************************************************************************************************\n")
cat("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
cat("*****************************************************************************************************************************************************\n")
cat("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
cat("*****************************************************************************************************************************************************\n")			
}



pathBase <- setPathBase(pathZero, idUsuario, ejecucion)

input.files <- setInputFiles(2, fileNameGrafoFuncional, fileNameGrafoSimilitudes, pathBase, graphPrefix)

gas <- getAtributtedStructuralGraph(input.files[1], yed_gas) #initial_as_graph_file= input.files[1]  # AS_G (attributed structural graph)

app_vertex <- extractAppVertex(gas, stv[2])
labs <- extractLabs(2, gas, app_vertex)
lista_apps <- labs[[2]]

gs <- getAttributeSimilarityGraph(input.files[2], yed_gs) #initial_s_graph_file=input.files[2] # S_G (similarities graph)

gu <- mergeGasAndGs(gas, gs)

outputGraphs_filenames <- c("gas_graph.graphml", "gs_graph.graphml", "gu_graph.graphml") 

saveGraphs(gas, gs, gu, outputGraphs_filenames, pathBase, graphPrefix)


if(TRUE){

rwParam <- setRandomWalkParameters(2, lengthWalk, restartProbability) #rwParam[1] #longitud de path maxima #rwParam[2] # restart probability
fnParam <- setOutputDistanceFilenameParameters(2, distPrefix, "dist-") #fnParam[1] # path de ficheros resultados distancias #fnParam[2] # prefijo de los ficheros resultados
cfnParam <- setOutputClusteringFilenameParameters(2, clustPrefix, "apps-clustering-")

out.Distance.files <- buildALLOutputDistanceFileNames(4, pathBase, fnParam[1], fnParam[2], rwParam[1], iter)

out.Clustering.files <- buildOutputClusteringFilenames(3, cfnParam[1], "weight_register.csv", "adjmatrix.csv", "intracluster_similarity.csv", pathBase)


start_time3 <- Sys.time()

weight_register <- cbind(weight_names)
col_names_weight_register <- c("weight_names")
colnames(weight_register) <- col_names_weight_register

iter <- 0
new_weights <- c(wv_sf, wv_st)  # dicen que no es muy eficiente aniadir filas a una matriz durante la ejecucion, que se tiene que copiar un objeto nuevo cada vez... de momento lo hacemos a lo cutre, luego lo optimizo si eso
col_names_weight_register <- update_col_names_weight_register(col_names_weight_register, iter)
weight_register <- update_weight_register(weight_register, new_weights, iter, col_names_weight_register)

saveWeights2csv(weigth_register, out.Clustering.files[1], weight_names)

d_vector <- NULL

{
adj_matrix <- as_adjacency_matrix(gu, type = "both", attr = NULL, edges = FALSE, names = TRUE, sparse = FALSE)
write.table(adj_matrix, file=out.Clustering.files[2],  row.names = TRUE, col.names= FALSE,  sep=";")

#adj_matrix[rownames(adj_matrix)==mymedoids[5]]   <<-- devuelve la fila correspondiente de ese medoid, de la adj_matrix

#lista_att <- extractAttVertex(gu, "fea", "tag")
lista_att_fea <- extractAttVertex(gu, ntv[3]) # aqui lista los vertex que son features... en el caso de estudio son 161 nodos... pero hay que agruparlos luego a 6
lista_att_tag <- extractAttVertex(gu, ntv[2]) # aqui lista los vertex que son tags... en el caso de estudio 8

lista_att <- c(lista_att_fea, lista_att_tag)

if(DepuroAjustePesos){
cat("lista_att *****************************************************************************************************************\n")
print(lista_att)
}

att_vertex <-rep(0, times=dim(adj_matrix)[2])

att_vertex[lista_att] = 1  # me quedo con un vector que tiene a 1 las columnas que se corresponden con nodos atributo, bien feature bien tag

if(DepuroAjustePesos){
cat("att_vertex *****************************************************************************************************************\n")
print(att_vertex)
}

}


if(PruebasIntegracion | !CalculoSimilarityMatrix){ITERACIONES <- 1} else {ITERACIONES <- ITERACIONES_MAX}

for(i in 1:ITERACIONES){

cat("************************************************************************************\n")
cat("************************************************************************************\n")
cat("************************************************************************************\n")
cat("************************************************************************************\n")
cat("************************************************************************************\n")
cat("*****                               ITERACION ", i ,"                              *****\n")
cat("************************************************************************************\n")
cat("************************************************************************************\n")
cat("************************************************************************************\n")
cat("************************************************************************************\n")

out.Distance.files <- buildALLOutputDistanceFileNames(4, pathBase, fnParam[1], fnParam[2], rwParam[1], iter)


{
#start_time2 <- Sys.time()

#P2 <- computeProbabilityMatrix(gas, ntv, ftv, ttv, w_ss, wv_sf, wv_st)
#D2 <- computeSimilarityMatrix(P2, rwParam[1], rwParam[2], labs[[1]])
#D2_apps <- computeSimilarityMatrixApps(D2, app_vertex, labs[[2]])

#end_time2 <- Sys.time()
#time_taken2 <- end_time2 - start_time2
	
#mydiftime2 <- difftime(end_time2, start_time2, units = "secs")
#cat("time taken raw: ", time_taken2, "\n")
#cat("time taken (secs): ", mydiftime2, "\n")

#saveSimilarityMatrix2csv(D2, out.Distance.files[1])
#saveSimilarityMatrixApps2csv(D2_apps, out.Distance.files[2])
}


######################################################################################################################################################
####################                                     CALCULO APP SIMILARITY MATRIX
######################################################################################################################################################

if(CalculoSimilarityMatrix){

	P3 <- computeProbabilityMatrix(gu, ntv, ftv, ttv, w_ss, wv_sf, wv_st)
	D3 <- computeSimilarityMatrix(P3, rwParam[1], rwParam[2], labs[[1]])
	D3_apps <- computeSimilarityMatrixApps(D3, app_vertex, labs[[2]])

	end_time3 <- Sys.time()
	mydiftime3 <- difftime(end_time3, start_time3, units = "secs")
	cat("Similarity matrix -- iter: ", iter, ", time taken raw: ", end_time3 - start_time3, "time taken (sec): ", mydiftime3, "time taken (min): ", mydiftime3/60, "\n")
		
	saveSimilarityMatrix2csv(D3, out.Distance.files[3])
	saveSimilarityMatrixApps2csv(D3_apps, out.Distance.files[4])

} else {

	SimilarityMatrixAux_filename <- paste(pathBase, distPrefix, "dist-3a3-0.csv", sep="", collapse = NULL)
	D3_apps <- readSimilarityMatrixApps4csv(SimilarityMatrixAux_filename, lista_apps)

}


######################################################################################################################################################
###################                                CALCULO OPTIMUM_K, MEDOIDS AND CLUSTERING 
######################################################################################################################################################


myappclust <- computePAMClusteringFromSimilarityMatrix(D3_apps, lista_apps, sigma)

mymedoids <- myappclust$medoids

if(DepuroAjustePesos){
cat("myappcluster *****************************************************************************************************************\n")
print(myappclust$cluster)
}

#write.table(myappclust$cluster, file=newfilename,  row.names = TRUE, col.names= FALSE,  sep=";")
appClustering_csvfilename <- paste(out.Clustering.files[3], "apps-clustering-","iter", iter, ".csv", sep="", collapse = NULL)
saveAppClustering2csv(myappclust$cluster, appClustering_csvfilename)


######################################################################################################################################################

######################################################################################################################################################
###################                                           CALCULO SIMILITUD INTRACLUSTER     
######################################################################################################################################################

clus_matrix <- readAppClustering4csv(appClustering_csvfilename)

d <- computeIntraclusterSimilarity(clus_matrix, D3_apps)

d_vector <- c(d_vector, d)

write.table(d_vector, file=out.Clustering.files[4],  row.names = TRUE, col.names= FALSE,  sep=";")

#cat("********************      vector de intracluster similarities     ***************************\n")
#print(d_vector)

######################################################################################################################################################


######################################################################################################################################################
###################                                           AJUSTE DE LOS PESOS     
######################################################################################################################################################

N <- length(unique(clus_matrix[,"V2"], incomparables = FALSE))

vector_suma <- rep(0, times=dim(adj_matrix)[2])

if(DepuroAjustePesos){
	cat("EXTRAIGO ADJ MATRIX DE LAS MEDOIDS y RESTO COMPONENTES CLUSTER SUYO\n")
	missumas <- rep(0, times=dim(adj_matrix)[2])
}

# recorro todos los clusters usando i
for(i in 1:N) {

	vector_suma_cluster <- rep(0, times=dim(adj_matrix)[2])

	mm <- adj_matrix[rownames(adj_matrix)==mymedoids[i]]

	if(DepuroAjustePesos){
		cat("mm: extracto de adj_matrix de medoid ", mymedoids[i] ,"*****************************************************************************************************************\n")
		print(mm)
	}
	
	inter <- (mm & att_vertex)
	target <- as.numeric(inter)

	cluster <- clus_matrix[clus_matrix[, "V2"] == i,]

	if(DepuroAjustePesos){
		cat("cluster",  i , " *****************************************************************************************************************\n")
		print(cluster)	
	}
	
	M <- dim(cluster)[1]

	##OJO, he modificado esto para que solo sume si hay mas de un nodo en el cluster... 
	# los autores originales calculan los centroids... pero nosotros trabajamos con medoids
	# no tiene sentido comparar el medoid consigo mismo... 
		
	for(j in 1:M){
	
		if(DepuroAjustePesos){
			cat("QUE COMPARO medoid vs elemento: ", mymedoids[i], " vs ", toString(cluster[j,"V1"]) , " \n")
		}
		
		if(!(toString(cluster[j,"V1"])==mymedoids[i]))
		{	
			nn <- adj_matrix[rownames(adj_matrix)==cluster[j,"V1"]]

			if(DepuroAjustePesos){
				cat("nn: extracto adj_matrix de ", toString(cluster[j,"V1"]) , "*****************************************************************************************************************\n")
				print(nn)
			}
			
			check <- as.numeric(nn & att_vertex)
			
			if(DepuroAjustePesos){
				cat("check: interseccion medoid ", mymedoids[i], " vs elemento ", toString(cluster[j,"V1"]) , "*****************************************************************************************************************\n")
				print(as.numeric(target & check))
			}
			
			vector_suma_cluster <- vector_suma_cluster + as.numeric(target & check)		
						
		} else {
		
			if(DepuroAjustePesos){
				cat("No comparo con medoid \n")
			}
		
		}		
	}
	
	if(DepuroAjustePesos & sum(vector_suma_cluster)>0){
				cat("*****************************************************************************************************************************************************\n")
				cat("*****************************************************************************************************************************************************\n")
				cat("*****************************************************************************************************************************************************\n")
				cat("vector suma de cluster ", i , "*****************************************************************************************************************\n")
				print(vector_suma_cluster)
				
				missumas <- cbind(missumas, vector_suma_cluster)
	}
	
	vector_suma <- vector_suma + vector_suma_cluster	
		
}


if(DepuroAjustePesos){
	cat("vector suma  *****************************************************************************************************************\n")
	print(vector_suma)
	
	write.table(missumas, file="./missumas.csv",  row.names = TRUE, col.names= FALSE,  sep=";")
}

vector_feature_votes <- matrix(data=rep(0, times=length(ftv)), nrow=1,ncol=length(ftv))
for (i in 1:length(vector_feature_votes))
{
	lista_feature_type_vertex <- extractFeatureTypeVertex(gu,ftv[i])
	# sumo los votos correspondientes a cada tipo de feature type
	vector_feature_votes[i] <- sum(vector_suma[lista_feature_type_vertex])
}

vector_tag_votes <- matrix(data=rep(0, times=length(ttvr)), nrow=1,ncol=length(ttvr))
for (i in 1:length(vector_tag_votes))
{
	lista_tag_type_vertex <- extractTagTypeVertex(gu,ttvr[i])
	# sumo los votos correspondientes a cada tipo de feature type
	vector_tag_votes[i] <- sum(vector_suma[lista_tag_type_vertex])
}



#vector_tag_votes <- c(vector_suma[lista_att_tag], rep(0, times=length(ttv)-length(vector_suma[lista_att_tag])))
vector_tag_votes <- c(vector_tag_votes, rep(0, times=length(ttv)-length(ttvr)))


vector_votes <- c(vector_feature_votes, vector_tag_votes)

#vector_votes <- length(vector_votes)*vector_votes/sum(vector_votes)
vector_votes <- (length(ftv)+length(ttvr))*vector_votes/sum(vector_votes)

if(DepuroAjustePesos2){
cat("vector feature votes y tag votes, lengths: ",length(vector_feature_votes) ," y ", length(vector_tag_votes),"\n")
print(vector_feature_votes)
print(vector_tag_votes)
}


if(DepuroAjustePesos){
cat("vector_votes antes de modificar\n")
print(vector_votes)
cat("length vector_votes ", length(vector_votes), "\n")
cat("length vector_votes EFECTIVOS", (length(ftv)+length(ttvr)), "\n")
}
	

if(DepuroAjustePesos2){
cat("new_weights antes de modificar\n")
print(new_weights)
cat("length new_weights ", length(new_weights), "\n")
}

new_weights <- 1/2*(new_weights + vector_votes) # cutre ahora para simular

if(DepuroAjustePesos2){
print(new_weights)
}

# ajusto el numero de iteracion
iter <- iter + 1

# Aniado la nueva lista de valores de los pesos a la matriz-registro de pesos
col_names_weight_register <- update_col_names_weight_register(col_names_weight_register, iter)
weight_register <- update_weight_register(weight_register, new_weights, iter, col_names_weight_register)
# la guardo en el csv
saveWeights2csv(weigth_register, out.Clustering.files[1], weight_names)



# actualizo las variables pesos 
 wv_sf <- new_weights[1:6]
 wv_st <- new_weights[7:46]

}

## OTRA ITERACION de la calculo de la matriz de similitudes con los nuevos pesos 

}