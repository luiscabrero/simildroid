####################################################################
#
# Necesita que se ejecute antes dataTypes.R, para "importar" N_ft y N_tt
#
####################################################################

weight_names <- c("w_sf_1", "w_sf_2", "w_sf_3", "w_sf_4", "w_sf_5", "w_sf_6", "w_st_1","w_st_2", "w_st_3", "w_st_4", "w_st_5", "w_st_6", "w_st_7", "w_st_8", "w_st_9", "w_st_10", "w_st_11", "w_st_12", "w_st_13", "w_st_14", "w_st_15", "w_st_16", "w_st_17", "w_st_18", "w_st_19", "w_st_20", "w_st_21", "w_st_22", "w_st_23", "w_st_24", "w_st_25", "w_st_26", "w_st_27", "w_st_28", "w_st_29", "w_st_30", "w_st_31", "w_st_32", "w_st_33", "w_st_34", "w_st_35", "w_st_36", "w_st_37", "w_st_38", "w_st_39", "w_st_40")

{
w_sf_1 = 1 # dev_type

w_sf_2 = 1 # app_name
w_sf_3 = 1 # app_category
w_sf_4 = 1 # app_permissions
w_sf_5 = 1 # app_author

w_sf_6 = 1 # res_type

w_st_1 = 1
w_st_2 = 1
w_st_3 = 1
w_st_4 = 1
w_st_5 = 1
w_st_6 = 1
w_st_7 = 1
w_st_8 = 1
w_st_9 = 1
w_st_10 = 1
w_st_11 = 1
w_st_12 = 1
w_st_13 = 1
w_st_14 = 1
w_st_15 = 1
w_st_16 = 1
w_st_17 = 1
w_st_18 = 1
w_st_19 = 1
w_st_20 = 1
w_st_21 = 1
w_st_22 = 1
w_st_23 = 1
w_st_24 = 1
w_st_25 = 1
w_st_26 = 1
w_st_27 = 1
w_st_28 = 1
w_st_29 = 1
w_st_30 = 1
w_st_31 = 1
w_st_32 = 1
w_st_33 = 1
w_st_34 = 1
w_st_35 = 1
w_st_36 = 1
w_st_37 = 1
w_st_38 = 1
w_st_39 = 1
w_st_40 = 1

}
#sw = 0


w_ss=1 #peso enlaces estructurales

#wv_sf <- matrix(data=c(w_sf_1, w_sf_2, w_sf_3, w_sf_4, w_sf_5, w_sf_6), nrow=1,ncol=(N_ft))
wv_sf <- matrix(data=c(rep(1, times=N_ft)), nrow=1,ncol=(N_ft))

w_st=1 # peso enlaces de nodo estructural a tag

#wv_st <- matrix(data=c(w_st_1, w_st_2, w_st_3, w_st_4, w_st_5, w_st_6, w_st_7, w_st_8, w_st_9, w_st_10, w_st_11, w_st_12, w_st_13, w_st_14, w_st_15, w_st_16, w_st_17, w_st_18, w_st_19, w_st_20, w_st_21, w_st_22, w_st_23, w_st_24, w_st_25, w_st_26, w_st_27, w_st_28, w_st_29, w_st_30, w_st_31, w_st_32, w_st_33, w_st_34, w_st_35, w_st_36, w_st_37, w_st_38, w_st_39, w_st_40),nrow=1,ncol=(N_tt))
wv_st <- matrix(data=c(rep(1, times=N_ttr), rep(0, times=(N_tt-N_ttr))), nrow=1, ncol=(N_tt))

#wv <- matrix(data=c(w_ss, w_st_1, w_st_2, w_st_3, w_st_4, w_st_5, w_st_6, w_st_7, w_st_8, w_st_9, w_st_10, w_st_11, w_st_12, w_st_13, w_st_14, w_st_15, w_st_16, w_st_17, w_st_18, w_st_19, w_st_20, w_st_21, w_st_22, w_st_23, w_st_24, w_st_25, w_st_26, w_st_27, w_st_28, w_st_29, w_st_30, w_st_31, w_st_32, w_st_33, w_st_34, w_st_35, w_st_36, w_st_37, w_st_38, w_st_39, w_st_40, w_sf_1, w_sf_2, w_sf_3, w_sf_4, w_sf_5, w_sf_6),nrow=1,ncol=(1+N_tt+N_ft))



