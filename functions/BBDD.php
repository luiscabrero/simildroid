<?php
    define("DBHOST","localhost");
    define("DATABASE","mydb");
    define("USER","root");
    define("PASSWORD","admin");
    
    class BBDD{
        public $conn = null;
        function __construct(){
            $this->conn = new mysqli("localhost", "root", "admin", "mydb");
            // Comprobacion de conexion 
            if ($this->conn->connect_error) {
                die("Error en la conexion a MySQL: " . $this->conn->connect_error);
            }
        }
        
        function obtenerTerminales($nick){
            //preparacion de la consulta
            if($stmt = $this->conn->prepare("SELECT * FROM Terminal WHERE Usuario_nick = ?")){
                $stmt->bind_param("s", $nick);
                
                //ejecutamos la consulta
                $stmt->execute();
                
                //obtenemos la lista de resultados
                $resultado = $stmt->get_result();
                
                return $resultado;

                
            }else{
                printf("Error al buscar Terminales de %s",$nick);
                return null;
            }
        }
        function obtenerNombreTerminal($idTerminal){
            //preparacion de la consulta
            if($stmt = $this->conn->prepare("SELECT nombre FROM Terminal WHERE idTerminal = ?")){
                $stmt->bind_param("i", $idTerminal);
                
                //ejecutamos la consulta
                $stmt->execute();
                
                //obtenemos la lista de resultados
                $resultado = $stmt->get_result();
                
                return $resultado;

                
            }else{
                printf("Error al buscar Terminales de %s",$nick);
                return null;
            }
        }
        function obtenerAplicaciones($idTerminal){
            //preparacion de la consulta
            if($stmt = $this->conn->prepare("SELECT * FROM Aplicacion WHERE Terminal_idTerminal = ?")){
                $stmt->bind_param("i", $idTerminal);
                
                //ejecutamos la consulta
                $stmt->execute();
                
                //obtenemos la lista de resultados
                $resultado = $stmt->get_result();
                
                return $resultado;

                
            }else{
                printf("Error al buscar Terminales de %s",$nick);
                return null;
            }
        }
         
        function obtenerPermisos($idTerminal,$aplicacion){
            //preparacion de la consulta
            if($stmt = $this->conn->prepare("SELECT Permiso_nombre FROM Aplicacion_Permiso WHERE Aplicacion_nombre = ? AND Aplicacion_Terminal_idTerminal = ?")){
                $stmt->bind_param("ss", $aplicacion, $idTerminal);
                
                //ejecutamos la consulta
                $stmt->execute();
                
                //obtenemos la lista de resultados
                $resultado = $stmt->get_result();
                
                return $resultado;

                
            }else{
                printf("Error al buscar Permisos de %s",$nick);
                return null;
            }
        }
        function obtenerEmail($nick){
            //preparacion de la consulta
            if($stmt = $this->conn->prepare("SELECT email FROM Usuario WHERE nick = ?")){
                $stmt->bind_param("s", $nick);
                
                //ejecutamos la consulta
                $stmt->execute();
                
                //obtenemos la lista de resultados
                $resultado = $stmt->get_result();
                
                return $resultado;

                
            }else{
                printf("Error al buscar email de %s",$nick);
                return null;
            }
        }
        function obtenerEtiquetas($idTerminal,$aplicacion){
            //preparacion de la consulta
            if($stmt = $this->conn->prepare("SELECT Etiqueta_nombre FROM Aplicacion_Etiqueta WHERE Aplicacion_nombre = ? AND Aplicacion_Terminal_idTerminal = ?")){
                $stmt->bind_param("ss", $aplicacion, $idTerminal);
                
                //ejecutamos la consulta
                $stmt->execute();
                
                //obtenemos la lista de resultados
                $resultado = $stmt->get_result();
                
                return $resultado;

                
            }else{
                printf("Error al buscar Permisos de %s",$nick);
                return null;
            }
        }
        
        function almacenarEtiqueta($idTerminal,$aplicacion,$etiqueta){
            if($stmt = $this->conn->prepare("INSERT INTO Aplicacion_Etiqueta VALUES(?,?,?)")){
                if(!$stmt->bind_param("sss", $aplicacion,$idTerminal,$etiqueta))
                    print("Error al insertar en Aplicacion_Etiqueta");
                //ejecutamos la consulta
                if(!$stmt->execute())
                    print("No se puede ejecutar la consulta almacenarEtiqueta".$stmt->error);
            }else{
                //print(" (X) ERRORRRRRRRRR<br>");
                return null;
            }
        }
        function almacenarResolucionR($nick, $resolucionInicial){
            if($stmt = $this->conn->prepare("INSERT INTO Resolucion VALUES(?,?,null)")){
                if(!$stmt->bind_param("ss", $nick,$resolucionInicial))
                    print("Error al insertar en Resolucion");
                //ejecutamos la consulta
                if(!$stmt->execute())
                    print("No se puede ejecutar la consulta almacenarResolucionR".$stmt->error);
            }else{
                //print(" (X) ERRORRRRRRRRR<br>");
                return null;
            }
        }

        function almacenarTarea($nick){
 	    $fechaGregoriana = getdate(); //obtenemos fecha para crear el fichero personalizado

            $fechaJuliana = gregoriantojd($fechaGregoriana["mon"] ,$fechaGregoriana["mday"] ,$fechaGregoriana["year"]);
            if($stmt = $this->conn->prepare("INSERT INTO Tarea VALUES(?, \"NO\", ?)")){
                if(!$stmt->bind_param("ss",$nick,$fechaJuliana))
                    return "a";
                if(!$stmt->execute())
		    return $stmt->error;
            }else{
                return "c";
            }
	    return "d";
        } 
        function eliminarTarea($nick){
            if($stmt = $this->conn->prepare("DELETE FROM Tarea WHERE Usuario_nick = ?")){
                if(!$stmt->bind_param("s",$nick))
                    return "a";
                if(!$stmt->execute())
		    return $stmt->error;
            }else{
                return "c";
            }
	    return "d";
        } 
        function obtenerTareas(){
            
            if($stmt = $this->conn->prepare("select Usuario_nick from Tarea WHERE terminado=\"NO\" ORDER BY fecha;")){
                //$stmt->bind_param("s", $nick);
                
                //ejecutamos la consulta
                if(!$stmt->execute())
                    print("No se puede ejecutar la consulta");
                //obtenemos la lista de resultados
                $resultado = $stmt->get_result();
                
                return $resultado;

                
            }else{
                printf("Error al buscar Aplicaciones de %s",$nick);
                return null;
            }
        }         
        function almacenarEtiquetaTerminal($idTerminal,$etiqueta){
            if($stmt = $this->conn->prepare("INSERT INTO Terminal_Etiqueta VALUES(?,?)")){
                if(!$stmt->bind_param("ss",$idTerminal,$etiqueta))
                    print("Error al insertar en Terminal_Etiqueta");
                //ejecutamos la consulta
                $stmt->execute();
            }else{
                return null;
            }
        } 

     
        function obtenerTodasAplicacionesPorUsuario($nick){
            
            if($stmt = $this->conn->prepare("SELECT DISTINCT A.nombre, A.paquete, T.idTerminal FROM Aplicacion A JOIN Terminal T WHERE A.Terminal_idTerminal = T.idTerminal AND T.Usuario_nick=?")){
                $stmt->bind_param("s", $nick);
                
                //ejecutamos la consulta
                $stmt->execute();
                
                //obtenemos la lista de resultados
                $resultado = $stmt->get_result();
                
                return $resultado;

                
            }else{
                printf("Error al buscar Aplicaciones de %s",$nick);
                return null;
            }
        }
        
        function obtenerPaquetesSinCategoria(){
            
            if($stmt = $this->conn->prepare("SELECT Aplicacion_nombre from Aplicacion_Categoria WHERE nombre IS NULL")){
                //$stmt->bind_param("s", $nick);
                
                //ejecutamos la consulta
                if(!$stmt->execute())
                    print("No se puede ejecutar la consulta obtenerPaquetesSinCategoria");
                //obtenemos la lista de resultados
                $resultado = $stmt->get_result();
                
                return $resultado;

                
            }else{
                printf("Error al buscar Aplicaciones de %s",$nick);
                return null;
            }
        }

        
        function guardarNuevasCategorias($aplicacion, $categoria){
            if($stmt = $this->conn->prepare("UPDATE Aplicacion_Categoria SET nombre=? WHERE Aplicacion_nombre=?")){
                if(!$stmt->bind_param("ss", $categoria, $aplicacion))
                    print("Error al insertar en Aplicacion_Categoria");
                //ejecutamos la consulta
                if(!$stmt->execute())
                    print("No se puede ejecutar la consulta obtenerPaquetesSinCategoria");
            }else{
                //print(" (X) ERRORRRRRRRRR<br>");
                return null;
            }
        }
        
        function obtenerTodasEtiquetas(){
            if($stmt = $this->conn->prepare("SELECT * from Etiqueta")){
                if(!$stmt->execute())
                    print("No se puede ejecutar la consulta obtenerPaquetesSinCategoria");
                //obtenemos la lista de resultados
                $resultado = $stmt->get_result();
                
                return $resultado;
            }else{
                return null;
            }
        }
        function obtenerEtiquetasPorUsuario($nick){
            if($stmt = $this->conn->prepare("select distinct Etiqueta_nombre FROM Terminal T JOIN ((select * from (Select Terminal_idTerminal, Etiqueta_nombre from Terminal_Etiqueta UNION Select Aplicacion_Terminal_idTerminal, Etiqueta_nombre from Aplicacion_Etiqueta ) PE) ) PE ON T.idTerminal = PE.Terminal_idTerminal WHERE T.Usuario_nick = ?;")){
                if(!$stmt->bind_param("s", $nick))
                    print("Error en obtenerEtiquetasPorUsuario");
                //ejecutamos la consulta
		if(!$stmt->execute())
                    print("No se puede ejecutar la consulta obtenerEtiquetasPorUsuario");
                //obtenemos la lista de resultados
                $resultado = $stmt->get_result();
                
                return $resultado;
            }else{
                return null;
            }
        }
        function obtenerEtiquetasDelUsuario($nick){
            if($stmt = $this->conn->prepare("SELECT Etiqueta_nombre FROM Usuario_Etiqueta WHERE Usuario_nick = ?;")){
                if(!$stmt->bind_param("s", $nick))
                    print("Error en obtenerEtiquetasDelUsuario");
                //ejecutamos la consulta
		if(!$stmt->execute())
                    print("No se puede ejecutar la consulta obtenerEtiquetasDelUsuario");
                //obtenemos la lista de resultados
                $resultado = $stmt->get_result();
                
                return $resultado;
            }else{
                return null;
            }
        }
        function obtenerCategoria($aplicacion_paquete){
            if($stmt = $this->conn->prepare("SELECT * from Aplicacion_Categoria WHERE Aplicacion_nombre = ?")){
                if(!$stmt->bind_param("s", $aplicacion_paquete))
                    print("Error al buscar en Aplicacion_Categoria");
                if(!$stmt->execute())
                    print("No se puede ejecutar la consulta obtenerCategoria");
                //obtenemos la lista de resultados
                $resultado = $stmt->get_result();
                //print("EL RESULTADO ES: ".$resultado);
                return $resultado;
            }else{
                return null;
            } 
        }
        function asignarAutor($app_paquete, $autor){
            if($stmt = $this->conn->prepare("UPDATE Aplicacion SET autor=? WHERE paquete=?")){
                if(!$stmt->bind_param("ss", $autor, $app_paquete))
                    print("Error al modificar el autor de la app");
                //ejecutamos la consulta
                if(!$stmt->execute())
                    print("No se puede ejecutar la consulta asignarAutor");
            }else{
                return null;
            }
        }
        
        function insertarNuevoWearable($usuario, $nombre_terminal, $apps){
            if($stmt = $this->conn->prepare("SELECT MAX(idTerminal) AS id FROM Terminal")){
			    //ejecutamos la consulta
			    $stmt->execute();
			    $stmt->store_result();
			    //vincular las variables de resultado
			    $stmt->bind_result($idTerminal);
			    //obtenemos el valor
			    $stmt->fetch();
			}
			
			$idSiguienteTerminal = $idTerminal+1;
			if($stmt = $this->conn->prepare("INSERT INTO Terminal VALUES (?, ?, ?, 'wearable', ?)")){
			    if(!$stmt->bind_param("isss",$idSiguienteTerminal, $usuario,$nombre_terminal,json_encode($apps)))
			    	print("Error al crear la consulta para insertar terminal wearable.");
			    //ejecutamos la consulta
			    if(!$stmt->execute())
			        print("ERROR".$stmt->error);
					
			}else{
			    //problemas con la sentencia
			    print("Error al insertar el terminal wearable");
			}
        }
        
	function insertarNuevaEtiquetaUsuario($usuario, $etiqueta){
		if($stmt = $this->conn->prepare("INSERT INTO Etiqueta VALUES (?)")){
			if(!$stmt->bind_param("s",$etiqueta))
				print("Error al insertarNuevaEtiquetaUsuario1");
			if(!$stmt->execute())
				print("ERROR".$stmt->error);
		}

		if($stmt = $this->conn->prepare("INSERT INTO Usuario_Etiqueta VALUES (?, ?)")){
			if(!$stmt->bind_param("ss", $usuario,$etiqueta))
				print("Error al insertarNuevaEtiquetaUsuario3");

			if(!$stmt->execute())
				print("ERROR".$stmt->error);
		}
	}





        function terminarConexion(){
            $this->conn->close();
        }
        
    }

?>
