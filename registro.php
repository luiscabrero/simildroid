<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Registro- SimilDroid</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<link href="style/index.css" rel="stylesheet">
	</head>
	<body>
    <!--login modal-->
    <div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
    <?php if($_GET["error"] != null){ ?>
	<?php if($_GET["error"] == "emailExistente"){?>
	  <div class="alert alert-danger">
	  <strong>Cuidado!</strong> El email que has introducido ya est&aacute; cogido.
	  </div>
	<?php }?>    
	<?php if($_GET["error"] == "nickExistente"){?>
	  <div class="alert alert-danger">
	  <strong>Cuidado!</strong> El nick que has introducido ya est&aacute; cogido.
	  </div>
	<?php }?>   
	<?php if($_GET["error"] == "passError"){?>
	  <div class="alert alert-danger">
	  <strong>Cuidado!</strong> Las contrase&ntilde;as no coinciden.
	  </div>
	<?php }?>
     <?php }?> 
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="text-center"><img src="images/logoclrs.png" alt=""></h1>
            </div>
            <div class="modal-body">
            	<form class="form col-sm-12 center-block" action="functions/checkRegistro.php" method="POST">
                  <div class="form-group">
                    <input type="text" class="form-control input-lg" name="Nickname" placeholder="Nickname">
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control input-lg" name="Password" placeholder="Contrase&ntilde;a">
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control input-lg" name="Password2" placeholder="Repite contrase&ntilde;a">
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control input-lg" name="Email" placeholder="Correo electr&oacute;nico">
                  </div>
                  <div class="form-group">
                    <button class="btn btn-primary btn-lg btn-block" type=submit>Registro</button>
                  </div>
                </form>
            </div>
            <div class="modal-footer">
                <span class="pull-left"><a href="index.php">Volver atr&aacute;s</a></span>
            </div>
        </div>
      </div>
    </div>
	<!-- script references -->

	</body>
</html>
