	$(function(){
		$("#sortable1, #sortable2" ).sortable({
			connectWith: '.connectedSortable'
		});
	
		$("#formulario" ).submit(function( event ) {
			var newOrde1 = $(this).find("#sortable1").sortable('toArray').toString();
			$("#input1").attr("value",newOrde1);
			var newOrde2 = $(this).find("#sortable2").sortable('toArray').toString();
			$("#input2").attr("value",newOrde2);
		});
	});
