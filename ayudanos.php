<!DOCTYPE html>
<?php
	include './functions/BBDD.php';
	session_start();
	if($_SESSION["nick"] == null)
		header("Location: ../index.php?error=nCn");

?>
<html lang="es">
    <head>
    	<title>Ayudanos</title>
    	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!--[if IE]><link rel="shortcut icon" href="images/favicon.ico"><![endif]-->
		<link rel="icon" href="images/favicon.png">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="./style/plantilla.css" media="screen"/>
		<link rel="stylesheet" type="text/css" href="./style/aplicaciones.css" media="screen"/>
      	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>
    	<!--CABECERA-->
       <header>
		    <div class="cabecera container-fluid">
				<nav id="menu" class="navbar navbar-default">
			    	<div class="navbar-header">
			         	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			            	<span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>                        
			            </button>
			            <a  href="./home.php"><img class="navbar-brand logotipo" src="./images/logoclrs.png"></a>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
			        	<ul class="nav navbar-nav">
			            	<li><a href="home.php"><span class="glyphicon glyphicon-phone"> </span> Mis dispositivos</a></li>
			            	<li><a href="ayudanos.php"><span class="glyphicon glyphicon-bullhorn"> </span> Ayudanos a mejorar</a></li>
			            	<li><a href="analizar.php"><span class="glyphicon glyphicon-tasks"> </span> Realizar an&aacute;lisis de similitud</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
			                <li class="dropdown"><a href="home.php"><?php echo $_SESSION["nick"];?></a></li>
			            	<li><a href="functions/apagar.php"><span class="cerrar glyphicon glyphicon-off"></span></a></li>
			            </ul>
			        </div>
			    </nav>
			</div>
	    </header>
	    <!--FIN CABECERA-->
	
	    <!--CUERPO-->
	    <div id="cuerpo" class="container-fluid">

		    <div class="nuevo col-md-8 col-md-offset-1">

                <div class="container">
                    <h2>Ayudanos a mejorar</h2>
                    <br> 
                    <p class="col-md-9">
                    Si has descubierto algún error, tienes problemas al utilizar SimilDroid, o simplemente tienes un consejo que darnos para mejorar la aplicaci&oacute;n, por favor, ponte en contacto con nosotros enviando un correo a: <b>100303515@alumnos.uc3m.es</b> &oacute; <b>aigonzal@inf.uc3m.es</b>
                    </p>
                </div>
                
		    </div>
			
	    </div>
	    <!--FIN CUERPO-->
	        
	    <!--PIE DE PAGINA-->
	    <footer>
		</footer>
	    <!--FIN PIE DE PAGINA-->
    </body>
</html>
